 # A custom base image can be specified as a Docker build argument.
 # The commands below assume that the image is Alpine Linux based.
 # This allows easily swapping out e.g. `node:10-alpine` for `node:13-alpine`.
ARG BASE_IMAGE=node:lts-alpine

# Usually copying just the top-level `package.json` and `yarn.lock` is
# sufficient to run `yarn install`, but this repo has multiple npm sub-packages
# (managed by lerna.js), and so we need to recursively copy all `package.json`
# and `yarn.lock` files.
# We use a a bash script to do the recursive copying, because otherwise we would
# need to manually write `COPY` Dockerfile directives for each npm sub-package.
# We do this by using a multi-stage build, as that way we can easily copy all
# files (with `COPY . .`) (so we can recursively iterate them) without affecting
# the `yarn install` layer cache of the main builder image.

FROM $BASE_IMAGE AS package_json_cache
RUN apk add coreutils # Needed because alpine's busybox `cp` is too limited
WORKDIR /src
COPY . .
RUN mkdir /out \
  && find . -name "package.json" -o -name "yarn.lock" -o -name "lerna.json" | \
    xargs cp -v --parents -t /out

FROM $BASE_IMAGE AS builder
RUN apk add git python3 make g++
WORKDIR /src
COPY --from=package_json_cache /out .
RUN yarn install
COPY . .
RUN yarn build
RUN git show 'HEAD:.dockerignore' \
  | xargs -I '{}' sh -c 'if git ls-files | grep -q "{}"; then git checkout -- "{}"; fi' \
  && git update-index -q --really-refresh && git diff-index --exit-code HEAD
