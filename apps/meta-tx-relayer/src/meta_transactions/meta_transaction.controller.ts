import {
  Body,
  Controller,
  Post,
  UseFilters,
  UseInterceptors,
} from '@nestjs/common';
import { AppLogger } from 'src/app.logger';
import { HttpExceptionFilter } from 'src/shared/filters/http-exception.filter';
import { LoggingInterceptor } from 'src/shared/interceptors/logging.interceptors';
import { TimeoutInterceptor } from 'src/shared/interceptors/timeout.interceptors';
import { MetaTransactionCreateDTO } from './dto/create.dto';
import { MetaTransactionService } from './meta_transaction.service';
import { MetaTransactionModule } from './meta_transactions.module';

@Controller('/')
@UseFilters(new HttpExceptionFilter())
@UseInterceptors(LoggingInterceptor, TimeoutInterceptor)
export class MetaTransactionController {
  constructor(
    private readonly logger: AppLogger,
    private readonly metaTransactionService: MetaTransactionService,
  ) {
    this.logger = new AppLogger(MetaTransactionModule.name);
  }
  @Post()
  async create(@Body() dto: MetaTransactionCreateDTO): Promise<boolean> {
    this.logger.log(`Request Received >> ${JSON.stringify(dto, null, ' ')}`);
    await this.metaTransactionService.validatePayload(
      dto.payload,
      dto.asset,
      dto.network_id,
    );
    return true;
  }
}
