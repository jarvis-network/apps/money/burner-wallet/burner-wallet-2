import { Module } from '@nestjs/common';
import { AppLogger } from 'src/app.logger';
import { DAIModule } from 'src/dai/dai.module';
import { MetaTransactionController } from './meta_transaction.controller';
import { MetaTransactionService } from './meta_transaction.service';

@Module({
  imports: [DAIModule],
  providers: [MetaTransactionService, AppLogger],
  controllers: [MetaTransactionController],
})
export class MetaTransactionModule {}
