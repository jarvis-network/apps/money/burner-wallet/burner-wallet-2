import { Injectable } from '@nestjs/common';
import { InjectConfig } from 'nestjs-config';
import { AppLogger } from 'src/app.logger';
import { DAIService } from 'src/dai/dai.service';

@Injectable()
export class MetaTransactionService {
  constructor(
    private readonly logger: AppLogger,
    private readonly daiService: DAIService,
    @InjectConfig() private readonly config,
  ) {
    this.logger = new AppLogger(MetaTransactionService.name);
  }
  async validatePayload(payload: string, asset: string, networkId: number) {
    this.logger.log(`Request ${JSON.stringify(payload, null, ' ')}`);
    if (asset === 'DAI') {
      await this.daiService.validatePayload(payload, networkId);
    }
  }
}
