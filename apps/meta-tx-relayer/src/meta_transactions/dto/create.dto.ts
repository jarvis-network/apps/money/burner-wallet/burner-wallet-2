import { BadRequestException } from '@nestjs/common';
import { IsInt, IsString, ValidationArguments } from 'class-validator';

export class MetaTransactionCreateDTO {
  @IsString({
    message: (_: ValidationArguments) => {
      throw new BadRequestException('Payload is required.');
    },
  })
  payload: string;
  @IsInt()
  network_id: number;
  @IsString({
    message: (_: ValidationArguments) => {
      throw new BadRequestException('Payload is required.');
    },
  })
  asset: string;
}
