import exitHook from 'async-exit-hook';
import 'dotenv/config';
import 'reflect-metadata';
import { AppDispatcher } from './app.dispatcher';
import { AppLogger } from './app.logger';

const logger = new AppLogger('Index');
/**
 * 0 to turn off the limit
 * https://stackoverflow.com/questions/9768444/possible-eventemitter-memory-leak-detected/31443386
 */
process.setMaxListeners(0);

logger.log(`Start`);

const dispatcher = new AppDispatcher();

dispatcher
  .dispatch()
  .then(() => logger.log('Everything up running'))
  .catch(e => {
    logger.error(e.message, e.stack);
    process.exit(1);
  });

exitHook(callback => {
  dispatcher.shutdown().then(() => {
    logger.log('Graceful shutdown the server');
    callback();
  });
});
