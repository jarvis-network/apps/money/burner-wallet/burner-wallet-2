interface Config {
  port: number | string | undefined;
  host: string | null | undefined;
  nodeEnv: string | null | undefined;
  ethNode: string | null | undefined;
  daiAddress: string | null | undefined;
  uniSwapExchangeAddress: string | null | undefined;
  gasPrice: string | null | undefined;
  serverAddress: string | null | undefined;
  networkId: number | string | undefined;
  testnetKovanAddress: string | null | undefined;
}
export const config: Config = {
  nodeEnv: process.env.NODE_ENV || 'development',
  port: process.env.PORT || 3100,
  host: process.env.HOST || 'localhost',
  ethNode:
    process.env.ETH_NODE ||
    'wss://kovan.infura.io/ws/v3/06b92f48aafe41a8b43bea2a19c5424c',
  daiAddress:
    process.env.DAI_ADDRESS || '0x02e3139041b43a599a41ec8a27b83e5d0b35ae5f',
  uniSwapExchangeAddress:
    process.env.UNI_SWAP_EXCHANGE_ADDRESS ||
    '0x2a1530C4C41db0B0b2bB646CB5Eb1A67b7158667',
  gasPrice: process.env.GAS_PRICE || '30000000000',
  serverAddress:
    process.env.SERVER_ADDRESS || '0x35f98b1c70637492f9c21891daa92fcb06220e37', // Running server
  networkId: process.env.NETWORK_ID || 42,
  testnetKovanAddress:
    process.env.TESTNET_KOVAN_ADDRESS ||
    '0x4f96fe3b7a6cf9725f59d353f723c1bdb64ca6aa',
};

export default config;
