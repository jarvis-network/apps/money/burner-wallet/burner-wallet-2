// Read more: https://medium.com/metamask/eip712-is-coming-what-to-expect-and-how-to-use-it-bb92fd1a7a26
export class EIP712Domain {
  static getDomainTypes(): string[] {
    return ['bytes32', 'bytes32', 'bytes32', 'uint256', 'address'];
  }
  static getSignatureName(): string {
    return 'EIP712Domain(string name,string version,uint256 chainId,address verifyingContract)';
  }
}
