export class DaiUtils {
  static getMessageTypes(): string[] {
    return ['bytes32', 'address', 'address', 'uint256', 'uint256', 'bool'];
  }
  public static versionSignature = '\x19\x01';
  // bytes32 public constant PERMIT_TYPEHASH = keccak256("Permit(address holder,address spender,uint256 nonce,uint256 expiry,bool allowed)");
  public static PERMIT_TYPEHASH =
    '0xea2aa0a1be11a07ed86d755c93467f4f82362b452371d1ba94d1715123511acb';
}
