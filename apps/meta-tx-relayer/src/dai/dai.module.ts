import { AppLogger } from 'src/app.logger';
import { Module } from '@nestjs/common';
import { DAIService } from './dai.service';

@Module({
  providers: [DAIService, AppLogger],
  exports: [DAIService],
})
export class DAIModule {}
