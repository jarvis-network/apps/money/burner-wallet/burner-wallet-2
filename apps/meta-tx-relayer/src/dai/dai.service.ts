import {
  DAILib,
  PermitTypes,
  UniSwapExchangeLib,
  Web3Service,
} from '@jarvis/meta-tx-contracts';
import { HttpException, HttpStatus, Injectable } from '@nestjs/common';
import { InjectConfig } from 'nestjs-config';
import { AppLogger } from 'src/app.logger';
import { EIP712Domain } from 'src/utils/EIP712Domain.utils';
import { DaiUtils } from './dai.utils';

@Injectable()
export class DAIService {
  private readonly DAILib: DAILib;
  public readonly web3Service: Web3Service;
  private readonly uniSwapExchangeLib: UniSwapExchangeLib;
  private readonly numberTxFunds = 5; // Need more info
  private readonly testnetExRate = 100; // Need more info
  constructor(
    private readonly logger: AppLogger,
    @InjectConfig() private readonly config,
  ) {
    this.logger = new AppLogger(DAIService.name);
    // Web3 Instance
    this.web3Service = new Web3Service(this.config.get('app.networkId'));

    this.DAILib = new DAILib(
      this.config.get('app.daiAddress'),
      this.web3Service,
    );
    this.uniSwapExchangeLib = new UniSwapExchangeLib(
      this.config.get('app.uniSwapExchangeAddress'),
      this.web3Service,
    );
  }
  async validatePayload(payload: string, networkId: number): Promise<boolean> {
    if (networkId !== this.config.get('app.networkId')) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Invalid Network id',
        },
        HttpStatus.FORBIDDEN,
      );
    }

    const funcSignaturePayload = payload.substr(0, 10);
    /* ------------------------ Validate Permit Signature ----------------------- */
    const funcSignature = this.DAILib.getFunctionSiguature('permit');
    if (funcSignature !== funcSignaturePayload) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Signature of Permit function not correct',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    this.logger.log('Payload had valid signature');
    const hexInit = '0x';
    const payloadArguments = hexInit.concat(payload.substr(10));

    const permitInputArgs: PermitTypes = this.DAILib.decodePermitFunction(
      payloadArguments,
    );
    this.logger.log(
      `DecodeInput >> ${JSON.stringify(permitInputArgs, null, ' ')}`,
    );
    // Validate Permit Function Payload
    await this.validatePermitFuctionPayload(permitInputArgs);
    // Validate Balances
    // await this.checkBalance(permitInputArgs.holder);
    //
    await this.createVerificationMessage(permitInputArgs, networkId);
    return true;
  }

  async createVerificationMessage(
    args: PermitTypes,
    networkId: number,
  ): Promise<Uint8Array> {
    /* ----------------------------- Create message ----------------------------- */

    const messageTypes = DaiUtils.getMessageTypes();
    const messageParameters = [
      DaiUtils.PERMIT_TYPEHASH,
      args.holder,
      args.spender,
      args.nonce,
      args.expiry,
      args.allowed,
    ];
    const messageBody = this.web3Service.encodeParams(
      messageTypes,
      messageParameters,
    );
    const messagePayload = this.web3Service
      .keccak256(messageBody)
      .replace('0x', '');
    this.logger.log(`Message Digest >> ${messagePayload}`);

    /* ----------------------------------- End ---------------------------------- */

    /* -------------------------- Create Domain Message ------------------------- */
    const domainTypes = EIP712Domain.getDomainTypes();
    const domainParameters = [
      this.web3Service.keccak256(EIP712Domain.getSignatureName()),
      this.web3Service.keccak256(this.web3Service.utf8ToHex('Dai Stablecoin')),
      this.web3Service.keccak256(this.web3Service.utf8ToHex('1')),
      networkId,
      this.config.get('app.testnetKovanAddress'),
    ];
    const domainBody = this.web3Service.encodeParams(
      domainTypes,
      domainParameters,
    );
    const domainPayload = this.web3Service
      .soliditySha3(domainBody)
      .replace('0x', '');
    this.logger.log(`Domain Digest >> ${domainPayload}`);
    /* ----------------------------------- End ---------------------------------- */

    const versionSignature = this.web3Service.utf8ToHex(
      DaiUtils.versionSignature,
    );
    const digestBody = versionSignature.concat(domainPayload, messagePayload);
    const body = this.web3Service.soliditySha3(digestBody);
    this.logger.log(`Digest >> ${body}`);
    return new Uint8Array(Buffer.from(body.replace('0x', ''), 'hex'));
  }
  /**
   * Validate the Permit Function Inputs
   * @param {PermitTypes} permitInputArgs
   * @memberof DAIService
   */
  async validatePermitFuctionPayload(permitInputArgs: PermitTypes) {
    /* -------------------------------------------------------------------------- */
    /*                     Permit Function Payload Validation                     */
    /* -------------------------------------------------------------------------- */

    if (
      permitInputArgs.spender.toLowerCase() !=
      this.config.get('app.serverAddress')
    ) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Spender address different from server address',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    const holderNonce = await this.DAILib.getNonce(permitInputArgs.holder);
    this.logger.log(`Holder ${permitInputArgs.holder} Nonce: ${holderNonce}`);
    if (permitInputArgs.nonce != holderNonce) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Wrong Nonce',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    // TODO: Add type casting
    if (permitInputArgs.expiry !== '0') {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Wrong expiry',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    if (permitInputArgs.allowed != true) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Allowance disabled',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    // TODO: Add type casting
    if (
      (permitInputArgs.v === '27' || permitInputArgs.v === '28'
        ? true
        : false) != true
    ) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Recover Id wrong',
        },
        HttpStatus.FORBIDDEN,
      );
    }
    /* ----------------------------------- END ---------------------------------- */
  }

  /**
   * Check ETH, DAI Balance
   * @param {string} address
   * @memberof DAIService
   */
  async checkBalance(address: string) {
    // Get Holder ETH blance
    const holderETHBalance = await this.web3Service.getBalance(address);
    this.logger.log(`Holder ${address} Balance : ${holderETHBalance} wei`);

    // Get sending price
    // Setting this  zero to resolve the issue in next MR.
    const sendingCost = 0;

    // Get Holder DAI Balance
    const holderDAIBalance = await this.DAILib.getBalance(address);
    this.logger.log(`Holder ${address} Balance : ${holderDAIBalance}  DAI`);

    let minimalDaiBalance: any = 0;
    const globalNetworkId = this.config.get('app.networkId');
    if (globalNetworkId === 42) {
      minimalDaiBalance = this.numberTxFunds * this.testnetExRate * sendingCost;
    }

    if (globalNetworkId === 1) {
      minimalDaiBalance = await this.uniSwapExchangeLib.getTokenToEthOutputPrice(
        this.numberTxFunds,
        sendingCost,
      );
    }

    if (holderDAIBalance < minimalDaiBalance) {
      throw new HttpException(
        {
          status: HttpStatus.FORBIDDEN,
          error: 'Holder Dai balance  too low for withdraw gas',
        },
        HttpStatus.FORBIDDEN,
      );
    }
  }
}
