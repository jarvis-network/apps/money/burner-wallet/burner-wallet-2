import { Module } from '@nestjs/common';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { ConfigModule } from 'nestjs-config';
import * as path from 'path';
import { AppLogger } from './app.logger';
import { LoggingInterceptor } from './shared/interceptors/logging.interceptors';
import { TimeoutInterceptor } from './shared/interceptors/timeout.interceptors';
import { MetaTransactionModule } from './meta_transactions/meta_transactions.module';

@Module({
  imports: [
    ConfigModule.load(path.resolve(__dirname, '**/!(*.d).config.{ts,js}'), {
      modifyConfigName: name => name.replace('.config', ''),
    }),
    MetaTransactionModule,
  ],
  providers: [
    {
      provide: APP_INTERCEPTOR,
      useClass: LoggingInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: TimeoutInterceptor,
    },
    AppLogger,
  ],
})
export class AppModule {
  private logger = new AppLogger(AppModule.name);
  constructor() {
    this.logger.log('App Module Initialized');
  }
}
