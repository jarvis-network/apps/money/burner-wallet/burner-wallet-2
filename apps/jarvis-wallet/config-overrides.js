const { override, addWebpackModuleRule } = require('customize-cra');
const { addReactRefresh } = require('customize-cra-react-refresh');

module.exports = override(
  addWebpackModuleRule({test: /\.md$/, use: 'raw-loader'}),
  addReactRefresh({ disableRefreshCheck: true })
);
