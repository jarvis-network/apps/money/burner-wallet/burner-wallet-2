import React from 'react';
import BurnerUICore, { Page } from '@burner-wallet/ui-core';
import styled from 'styled-components';

import burnerComponents from './components/burner-components';
import Loading from './components/Loading';
import Template from './Template';

import internalPlugins from './internal-plugins';
import Header from './components/Header';
import Background, { BackgroundProvider } from './components/Background';
import { NotificationProvider, NotificationContext } from './components/Notification';
import Router from './router';
import media, { sizes } from './styles/media';

const Wrapper = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  position: relative;
  background: #FAFBFD;
  overflow: hidden;
  padding-top: ${ sizes.header.height }px !important;

  ${ media.tablet.width } {
    padding: 0 ${ sizes.page.padding }px;
  }

  ${ media.mobile.content } {
    padding: 0 ${ sizes.page.margin }px;
  }

  ${ media.mobile.default } {
    padding: 0;
  }
`;

export default class JarvisUI extends BurnerUICore {
  getPages(): Page[] {
    return []
  }

  getInternalPlugins() {
    return internalPlugins;
  }

  burnerComponents() {
    return burnerComponents;
  }

  router() {
    return <Router pluginData={this.state.pluginData} />;
  }

  content() {
    return (
      <Template theme={this.props.theme}>
        <NotificationProvider>
          <BackgroundProvider>
          <Loading />
          <NotificationContext />
          <Header />
          <Wrapper>
            {this.router()}
          </Wrapper>
          <Background />
          </BackgroundProvider>
        </NotificationProvider>
      </Template>
    );
  }
}
