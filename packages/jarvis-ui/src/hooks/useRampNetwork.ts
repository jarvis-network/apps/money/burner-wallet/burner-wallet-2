import { useBurner } from '@burner-wallet/ui-core';
import { getRampUrl } from '../models/ramp-network';

export default function useRampNetwork() {
  const { defaultAccount } = useBurner();

  const rampURL = getRampUrl({
    hostAppName: 'Jarvis Burner Wallet',
    hostLogoUrl: 'https://jarvis.network/assets/images/jarvis-logo.svg',
    hostApiKey: process.env.REACT_APP_RAMP_NETWORK_API_KEY,
    swapAsset: 'DAI',
    userAddress: defaultAccount
  });

  const openRamp = () => window.open(rampURL, '_blank');

  return {
    rampURL,
    openRamp
  };
}
