import { useLocation } from 'react-router-dom';

export const useCurrentRouteIsPublic = () => {
  // This hook triggers a re-render whever the location is
  // changed, even though we're not using its result.
  useLocation();
  return !localStorage.getItem('terms');
};
