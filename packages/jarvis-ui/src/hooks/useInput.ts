import { useState } from 'react';

interface UseInputProps<InputType> {
  initialValue: InputType;
  validate?: (value: InputType) => boolean;
  transform?: (value: InputType) => InputType;
}

export default function useInput<InputType>({
  initialValue,
  validate,
  transform
}: UseInputProps<InputType>): any {
  const [dirty, setDirty] = useState(false);
  const [value, setValue] = useState<InputType>(initialValue);
  const [invalid, setInvalid] = useState<boolean>(false);

  const handleChange = (value: InputType) => {
    if (!!transform)
      value = transform(value)

    if (!!validate) {
      setInvalid(!validate(value));
    }

    setValue(value);
  };

  return [
    {
      value,
      invalid: invalid && dirty,
      onChange: handleChange,
      onBlur: () => setDirty(true)
    },
    value,
    handleChange,
    invalid
  ];
};
