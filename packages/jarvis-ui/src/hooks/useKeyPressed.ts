import { useState, useEffect } from 'react';

export default function useKeyPressed(key: string): boolean {
  const [pressed, setPressed] = useState(false);

  const onDown = (event: KeyboardEvent) => {
    if (key.toLowerCase() == event.key.toLowerCase())
      setPressed(true)
  };

  useEffect(() => {
    window.addEventListener("keydown", onDown);
    return () => {
      window.removeEventListener("keydown", onDown);
    }
  }, [key]);

  return pressed
}
