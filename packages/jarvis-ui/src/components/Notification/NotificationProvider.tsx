import React, { useContext, useState } from 'react';
import { Notification } from './types';

interface NotificationProvider {
  pushNotification: (notification: Notification) => void;
  notification: Notification | null;
}

const NotificationContext = React.createContext<NotificationProvider>({
  pushNotification: () => {},
  notification: null
});

const Provider: React.FC = ({ children }) => {
  const [state, setState] = useState<Notification | null>(null);

  return (
    <NotificationContext.Provider value={{
      pushNotification: setState,
      notification: state
    }}>
      {children}
    </NotificationContext.Provider>
  );
};

export default Provider;

export const useNotification = () => {
  return useContext(NotificationContext);
};
