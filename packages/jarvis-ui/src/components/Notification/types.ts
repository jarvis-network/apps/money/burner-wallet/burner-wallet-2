import { EmojiKeys } from '../Emoji/types';

export type NotificationType = 'info' | 'warning';

export interface Notification {
  message: string;
  emoji: EmojiKeys;
  type: NotificationType;
  delayed?: boolean;
  onlyOnce?: boolean;
}
