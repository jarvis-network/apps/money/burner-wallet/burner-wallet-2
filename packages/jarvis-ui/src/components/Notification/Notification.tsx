import React, { useState, useEffect, useCallback } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import { Notification as NotificationProps, NotificationType } from './types';
import { useWindowSize } from '../../hooks/useWindowSize';
import media, { sizes } from '../../styles/media';
import Emoji from '../Emoji';

const AnimationContainer = styled(motion.div)`
  position: fixed;
  top: 50px;
  width: 100%;
  z-index: 12;
  display: flex;
  flex-direction: column;
  justify-content: center;

  ${ media.mobile.width } {
    padding: 0 ${ sizes.page.margin }px;
  }
`;

const Wrapper = styled.div<{ type: NotificationType }>`
  margin: 0 auto;
  max-width: 689.16px;
  height: 61px;
  width: 100%;
  background-color: white;
  border-left: 10px solid ${({ type }) => {
    switch (type) {
      case 'info':
        return '#ffc000';
      case 'warning':
        return '#ff0000';
    }
  }};
  display: flex;
  align-items: center;
  box-shadow: 2px 2px 7px -1px lightgray;
`;

const StyledEmoji = styled(Emoji)`
  font-size: 18;
  margin: 0 20px;
`;

const Message = styled.p`
  font-size: 14px;
`;

const Notification: React.FC<NotificationProps> = (props) => {
  const { type, message, emoji, delayed = false } = props;
  const [animation, setAnimation] = useState('visible');
  const { width, height } = useWindowSize();
  let offsetPosition = -200;

  const updatePosition = useCallback(() => {
    if ((width as number) <= sizes.page.width) {
      offsetPosition = (height as number) + 100;
    }
  }, []);

  useEffect(() => {
    setAnimation('visible');
  }, [props]);

  useEffect(() => {
    updatePosition();
  }, [width, height, offsetPosition]);

  const variants = {
    hidden: {
      opacity: 0,
      y: offsetPosition,
      transition: { delay: 3, duration: 0.25 }
    },
    visible: (delay: number) => ({ opacity: 1, y: 0, transition: { delay } }),
    hiddenImmediately: {
      opacity: 0,
      y: offsetPosition,
      transition: { duration: 0.2 }
    }
  };

  return (
    <AnimationContainer
      custom={delayed ? 2 : 0}
      drag="y"
      onDragEnd={
        (event, { point: { y }}) => {
          if (y < -3) {
            setAnimation('hiddenImmediately');
          }
        }
      }
      dragConstraints={{ bottom: 0 }}
      dragElastic={0.2}
      variants={variants}
      positionTransition
      initial="hidden"
      animate={animation}
      exit="exit"
      onAnimationComplete={() => setAnimation('hidden')}
    >
      <Wrapper type={type}>
        <StyledEmoji emoji={emoji} />
        <Message>{message}</Message>
      </Wrapper>
    </AnimationContainer>
  );
};

export default Notification;
