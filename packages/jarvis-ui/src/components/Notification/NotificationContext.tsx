import React, { useEffect, useState } from 'react';
import { useNotification } from './NotificationProvider';
import Notification from './Notification';

const NotificationContext: React.FC = () => {
  const { notification } = useNotification();
  const [state, setState] = useState(notification);

  useEffect(() => {
    if (notification?.onlyOnce) {
      const storage = JSON.parse(localStorage.getItem('notifications') || '{}');

      const serialized = JSON.stringify(notification);
      if (!storage[serialized]) {
        localStorage.setItem('notifications', JSON.stringify({
          ...storage,
          [serialized]: notification
        }));
        setState(notification);
      } else {
        // the component will rerender and if there is a notification in the state
        // (from the last time) it will pass it to the notification component
        setState(null);
      }
    } else {
      setState(notification);
    }
  }, [notification]);

  if (!state)
    return null;

  return <Notification {...state} />
};

export default NotificationContext;
