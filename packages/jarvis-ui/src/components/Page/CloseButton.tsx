import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faTimes } from '@fortawesome/free-solid-svg-icons'
import Button from '../Button';
import media from '../../styles/media';

const StyledButton = styled(Button)`
  width: 40px;
  height: 40px;
  position: absolute;
  right: 0;
  top: 0;
  color: #BEBEBE;

  ${ media.mobile.default } {
    position: fixed;
  }
`;

interface CloseButtonProps {
  to: string | (() => void);
}

const CloseButton: React.FC<CloseButtonProps> = ({ to }) => {
  const isLink = typeof to === 'string';

  return (
    <StyledButton
      background="transparent"
      className="page-title-close-button"
      {...{
        to: isLink ? to as string : undefined,
        onClick: !isLink ? to as () => void : undefined
      }}
    >
      <FontAwesomeIcon icon={faTimes} width={10} height={10} />
    </StyledButton>
  );
};

export default CloseButton;
