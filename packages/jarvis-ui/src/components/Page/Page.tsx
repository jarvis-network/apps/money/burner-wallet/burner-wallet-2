import React, { useEffect } from 'react';
import styled from 'styled-components';
import { PageProps as BurnerPageProps } from '@burner-wallet/types';
import { Scrollbars } from 'react-custom-scrollbars';
import media, { sizes } from '../../styles/media';
import PageTitleBar from './PageTitleBar';
import ErrorBoundary from './ErrorBoundary';

type PageVariant = 'fullcontent' | 'fullscreen';

const pageMargin = sizes.page.margin;

const PageContainer = styled.main<{ fullscreen?: boolean }>`
  display: flex;
  flex-direction: column;
  position: relative;
  z-index: ${({ fullscreen }) => fullscreen ? '10' : '2'};
  background: #fff;
  width: ${ sizes.page.width }px;
  min-width: 360px;
  height: ${ sizes.page.height }px;
  align-self: center;
  box-shadow: var(--page-box-shadow);
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;

  ${ media.mobile.default } {
    width: 100%;
    height: 100%;
    ${({ fullscreen }) => fullscreen ? 'position: fixed;' : ''}
    box-shadow: none;
  }

  ${ media.mobile.content } {
     width: 100%;
     min-width: auto;
  }
`;

const TitleBarContainer = styled.div`
  padding: ${ pageMargin }px ${ pageMargin }px 0 ${ pageMargin }px;
`;

const Content = styled.div<{ fullcontent: boolean }>`
  flex: 1;
  display: flex;
  flex-direction: column;
  ${({ fullcontent }) => `
    width: ${fullcontent ? '100%' : sizes.page.content.width + 'px' };
    padding: ${fullcontent ? '0' : `0 ${ pageMargin }px ${ pageMargin }px ${ pageMargin }px;` }
  `}
  margin: 0 auto;

  ${ media.mobile.content } {
    width: 100%;
  }
`;

interface PageProps extends BurnerPageProps {
  variants?: PageVariant[];
  className?: string;
  closeTo?: string | (() => void);
  scrollable?: boolean;
}

const Page: React.FC<PageProps> = ({
  children,
  title,
  variant,
  variants= [],
  className,
  closeTo,
  scrollable = false
}) => {
  if (variant)
    variants = [variant as PageVariant];

  const isFullscreen = variants.indexOf('fullscreen') > -1;
  const isFullcontent = variants.indexOf('fullcontent') > -1;

  useEffect(() => {
    if (title) {
      document.title = title;
    }
  }, [title]);

  return (
    <PageContainer className={className} fullscreen={isFullscreen}>
      {(title || closeTo) &&
        <TitleBarContainer>
          <PageTitleBar
            title={title}
            to={closeTo}
          />
        </TitleBarContainer>
      }
      <Content fullcontent={isFullcontent}>
        {
          scrollable ?
          <Scrollbars autoHide>
            <ErrorBoundary>
              {children}
            </ErrorBoundary>
          </Scrollbars>
          :
          <ErrorBoundary>
            {children}
          </ErrorBoundary>
        }
      </Content>
    </PageContainer>
  );
};

export default Page;
