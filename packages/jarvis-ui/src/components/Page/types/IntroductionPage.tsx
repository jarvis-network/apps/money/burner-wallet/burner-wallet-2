import React from 'react';
import ReactMarkdown from 'react-markdown';
import Page from '../Page';

import Scrollbars from 'react-custom-scrollbars';
import styled from 'styled-components';

const Intro = styled.div`
  margin-top: 30px;
  margin-bottom: 50px;
  font-size: 18px;
`;

const StyledScrollbars = styled(Scrollbars)`
  border: 1px solid #F1F1F1;
`;

const StyledReactMarkdown = styled(ReactMarkdown)`
  padding: 15px;
`;

interface IntroductionPageProps {
  title: string;
  intro: string;
  source: string;
}

const IntroductionPage: React.FC<IntroductionPageProps> = ({ title, intro, source: content }) => (
  <Page title={title} variant="fullscreen">
    <Intro>{ intro }</Intro>
    <StyledScrollbars>
      <StyledReactMarkdown source={content} escapeHtml={false} />
    </StyledScrollbars>
  </Page>
);

export default IntroductionPage;
