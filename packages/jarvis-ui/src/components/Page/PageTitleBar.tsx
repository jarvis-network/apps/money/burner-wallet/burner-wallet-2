import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import CloseButton from './CloseButton';

const TitleBar = styled.div<{ hasTitle: boolean }>`
  display: flex;
  height: ${({ hasTitle }) => hasTitle ? '64px' : '0'};
  padding: 0 4px;
  justify-content: center;
  align-items: center;
  position: relative;
`;

const Title = styled.h1`
  margin: 0;
  font-size: 22px;
`;

export interface PageTitleBarProps extends RouteComponentProps {
  title?: string;
  to?: string | (() => void);
}

const PageTitleBar: React.FC<PageTitleBarProps> = ({ title, to, history }) => (
  <TitleBar hasTitle={!!title}>
    {title && <Title>{title}</Title>}

    {(title || to) && <CloseButton to={to || (() => history.goBack())} />}
  </TitleBar>
);

export default withRouter(PageTitleBar);
