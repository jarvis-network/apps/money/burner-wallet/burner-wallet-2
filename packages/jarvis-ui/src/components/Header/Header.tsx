import React from 'react';
import styled from 'styled-components';
import { useLocation } from 'react-router-dom';
import media, { sizes } from '../../styles/media';
import LogoJarvisMainSVG from './logos_jarvis-logo-main.svg';
import LogoJarvisMainBlackSVG from './logos_jarvis-logo-main-black.svg';
import { useCurrentRouteIsPublic } from '../../hooks/useRoutesUtils';

const HeaderElement = styled.header<{ isPrivate: boolean; home: boolean; }>`
  position: absolute;
  display: flex;
  width: 100%;
  height: ${ sizes.header.height }px;
  align-items: center;
  padding: var(--page-margin);
  z-index: 1;
  background: ${({ isPrivate }) => isPrivate ? '#fff' : 'transparent' };

  ${ media.mobile.default } {
    justify-content: center;
    ${({ home }) => home ? 'background: #F9F9F9;' : ''}
  }
`;

const Logo = styled.img`
  width: 40px;

  ${ media.mobile.default } {
    width: 15px;
  }
`;

const Header: React.FC = () => {
    const isPrivate = !useCurrentRouteIsPublic();
    const { pathname } = useLocation();

    return (
        <HeaderElement isPrivate={isPrivate} home={pathname === '/' || pathname.indexOf('/finance/') === 0}>
            <Logo src={
                    isPrivate ?
                    LogoJarvisMainSVG :
                    LogoJarvisMainBlackSVG
                }
            />
        </HeaderElement>
    );
};

export default Header;
