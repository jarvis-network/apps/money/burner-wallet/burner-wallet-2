import React from 'react';
import { HistoryEvent } from '@burner-wallet/types';
import { DataProviders, useBurner } from '@burner-wallet/ui-core';
import Transaction from './Transaction';
import styled from 'styled-components';

const NoActivity = styled.div`
  text-align: center;
`;

const { History } = DataProviders;

interface TransactionsProps {
  limit?: number;
}

const Transactions: React.FC<TransactionsProps> = ({ limit }) => {
  const { defaultAccount } = useBurner();

  return (
    <History
      account={defaultAccount}
      render={(events: HistoryEvent[]) => {
        if (events.length === 0) {
          return (
            <NoActivity>No recent activity</NoActivity>
          );
        }

        return events
          .slice(0, limit)
          .map((event: HistoryEvent) => (
            <Transaction
              key={event.id}
              event={event}
              account={defaultAccount}
            />
          ))
      }}
    />
  );
};

export default Transactions;
