import React from 'react';
import { withRouter, RouteComponentProps } from 'react-router-dom';
import { HistoryEvent } from '@burner-wallet/types';
import styled from 'styled-components';
import { ContentContainer } from '../../styles/components';
import IconButton from '../Button/Buttons/IconButton';

const Content = styled.div`
  height: 80px;
  border-bottom: 1px solid #f1f1f1;
  width: 100%;
  display: flex;
  justify-content: center;
  cursor: pointer;
`;

const Wrapper = styled(ContentContainer)`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const IconContainer = styled.div`
  display: flex;
  height: 100%;
  align-items: center;
  margin-top: -2px;

  img {
    width: 24px;
  }
`;

const DetailsContainer = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
`;

const Title = styled.span`
  font-size: 14px;
  font-weight: bold;
`;

const Amount = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 15px;
  color: #777777;

  strong {
    color: #000;
  }
`;

interface TransactionProps extends RouteComponentProps {
  event: HistoryEvent;
  account: string;
}

const Transaction: React.FC<TransactionProps> = ({ event, account, history }) => {
  const didReceive = event.to.toLowerCase() === account.toLowerCase();
  const asset = event.getAsset();
  if (!asset)
    return null;

  const amountSign = didReceive ? '+' : '-';

  return (
    <Content onClick={() => history.push(`/receipt/${asset.id}/${event.tx}`)}>
      <Wrapper>
        <IconContainer>
          <IconButton background="transparent" icon={didReceive ? 'download' : 'send'} />
        </IconContainer>
        <DetailsContainer>
          <Title>
            { didReceive ? 'Received' : 'Sent' }
          </Title>
          <Amount>
            <span><strong>{ amountSign + parseFloat(asset.getDisplayValue(event.value)).toFixed(2) } Dai</strong></span>
            <span>{ `${amountSign}$${ asset.getUSDValue(event.value) }` }</span>
          </Amount>
        </DetailsContainer>
      </Wrapper>
    </Content>
  );
};

export default withRouter(Transaction);
