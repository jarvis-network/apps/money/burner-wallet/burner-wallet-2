import Background from './Background';
import BackgroundProvider, { useBackground } from './BackgroundProvider';
export default Background;
export { BackgroundProvider, useBackground };
