import React, { useContext, useState } from 'react';

interface BackgroundProvider {
  setOffset: (offset: string) => void;
  offset: string;
}

const BackgroundContext = React.createContext<BackgroundProvider>({
  setOffset: () => {},
  offset: '50%'
});

const Provider: React.FC = ({ children }) => {
  const [state, setState] = useState<string>('50%');

  return (
    <BackgroundContext.Provider value={{
      setOffset: setState,
      offset: state
    }}>
      { children }
    </BackgroundContext.Provider>
  );
};

export default Provider;

export const useBackground = () => {
  return useContext(BackgroundContext);
};
