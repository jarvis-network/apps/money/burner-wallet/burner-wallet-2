import React, { useEffect, useState } from 'react';
import styled from 'styled-components';
import BackgroundImage from './background.svg';
import { motion } from 'framer-motion';
import { useCurrentRouteIsPublic } from '../../hooks/useRoutesUtils';
import { useBackground } from './BackgroundProvider';

const BackgroundComponent = styled(motion.div)`
  min-height: 100vh;
  height: 100vh;
  min-width: 100%;
  background: url(${BackgroundImage}) no-repeat center center;
  background-size: cover;
  position: fixed;
  z-index: 0;
`;

const Background: React.FC = () => {
  const isPublic = useCurrentRouteIsPublic();
  const { offset } = useBackground();

  return (
    <BackgroundComponent
      animate={isPublic ? { y: '0' } : {
        y: offset,
        transition: {
          type: 'spring',
          damping: 10,
          stiffness: 50
        }
      }}
    />
  );
};

export default Background;
