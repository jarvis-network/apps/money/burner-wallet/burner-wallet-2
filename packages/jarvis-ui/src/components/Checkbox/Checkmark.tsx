import React from 'react'
import styled from 'styled-components'

const Icon = styled.svg`
  fill: none;
  stroke: white;
  stroke-width: 2px;
`

const Checkmark: React.FC = () => (
    <Icon viewBox="0 0 24 24">
        <polyline points="20 6 9 17 4 12" />
    </Icon>
);

export default Checkmark;
