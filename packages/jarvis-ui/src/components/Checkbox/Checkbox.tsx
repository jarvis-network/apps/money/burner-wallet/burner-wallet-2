import React, { useState } from 'react';
import styled from 'styled-components';

import Checkmark from './Checkmark';

interface CheckboxProps {
    checked: boolean;
    onChange: () => void;
}

const CheckboxContainer = styled.label`
    display: inline-block;
    vertical-align: middle;
`

const CheckboxInput = styled.input.attrs({ type: 'checkbox' })`
    border: 0;
    clip: rect(0 0 0 0);
    clippath: inset(50%);
    height: 1px;
    margin: -1px;
    overflow: hidden;
    padding: 0;
    position: absolute;
    white-space: nowrap;
    width: 1px;
`;

const VisibleCheckboxInput = styled.div<{ checked: boolean }>`
    display: inline-block;
    width: 16px;
    height: 16px;
    background: ${({ checked }) => (checked ? '#54FB77' : '#F9F9F9')}
    transition: all 150ms;

    ${CheckboxInput}:checked + & {
        box-shadow: 0 0 3px #36af50;
    }
`;

const Checkbox: React.FC<CheckboxProps> = ({ checked, onChange }) => (
    <CheckboxContainer>
        <CheckboxInput type="checkbox" checked={checked} onChange={() => onChange()} />
        <VisibleCheckboxInput checked={checked}>
            {checked && <Checkmark /> }
        </VisibleCheckboxInput>
    </CheckboxContainer>
);

export default Checkbox;
