export enum Icon {
  arrow,
  close,
  download,
  plus,
  scan,
  send
}

export type IconKeys = keyof typeof Icon;
