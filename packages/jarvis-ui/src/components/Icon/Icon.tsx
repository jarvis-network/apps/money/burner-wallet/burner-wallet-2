import React from 'react';
import { IconKeys } from './types';

interface IconProps {
  icon: IconKeys;
}

const Icon: React.FC<IconProps> = ({ icon, ...props }) => <img src={require(`../Icon/assets/${icon}.svg`)} {...props} />;

export default Icon;
