import React from 'react';
import { LinkButton, StandardButton } from './Buttons';
import { ButtonProps } from './types';

const Button: React.FC<ButtonProps> = (props) =>
  // @ts-ignore: prop 'to' is already passed; prop 'type' and the default button type prop are incompatible
  !!props.to ? <LinkButton {...props} /> : <StandardButton {...props} />;

export default Button;
