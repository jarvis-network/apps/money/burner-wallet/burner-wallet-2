import { ReactNode } from 'react';
import { ButtonProps as BurnerButtonProps } from '@burner-wallet/types';

type ButtonType = 'default' | 'success' | 'dark';

export interface ButtonProps extends BurnerButtonProps {
  background?: string;
  color?: string;
  rounded?: boolean;
  type?: ButtonType;
  children?: ReactNode;
  [key: string]: any;
}
