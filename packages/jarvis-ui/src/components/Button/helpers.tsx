import React from 'react';
import { ButtonProps } from './types';

export const generateButtonStyles = ({
  color,
  background
}: ButtonProps) => `
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  flex-wrap: wrap;
  font-size: 14px;
  text-align: left;
  padding: 12px 22px;
  font-size: 18px;
  font-weight: normal;
  border: none;
  background: ${background || '#f1f1f1'};
  text-decoration: none;
  color: ${color || '#000'};
  margin: 0;
  cursor: pointer;
  transition: all 150ms;

  &[type=success] {
    background: #54fb77;
    box-shadow: var(--page-box-shadow);
  }

  &[type=dark] {
    background: #404040;
    box-shadow: var(--page-box-shadow);
    color: #fff;
  }

  &[rounded] {
    borderRadius: 30px;
  }

  &[disabled] {
    cursor: not-allowed;
    background: #f2f2f2;
    color: #bfbfbf;
  }
`;
