import styled from 'styled-components';
import { Link } from 'react-router-dom';
import { ButtonProps } from '../types';
import { generateButtonStyles } from '../helpers';

export default styled(Link)<ButtonProps>`
  ${(props) => generateButtonStyles(props) }

  &:active {
    color: ${({ color }) => color};
  }
`;
