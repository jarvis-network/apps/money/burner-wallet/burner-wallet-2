import React, { ReactNode } from 'react';
import styled from 'styled-components';
import Button from '../Button';
import { ButtonProps } from '../types';
import Icon from '../../Icon';
import { IconKeys } from '../../Icon/types';
import IconButton from './IconButton';

const StyledButton = styled(Button)`
  border: 1px solid #f1f1f1;
  padding: 25px;
`;

const StyledIconButton = styled(IconButton)`
  max-width: 45px;
  max-height: 45px;
  margin-right: 25px;
`;

const Content = styled.div`
  flex: 1;
  font-size: 14px;
  display: flex;
  flex-direction: column;
`;

const Title = styled.span`
  font-weight: bold;
`;

const StyledIcon = styled(Icon)`
  width: 10px;
`;

interface CardButtonProps extends ButtonProps {
  title: string | ReactNode;
  subtitle?: string;
  leftButtonIcon?: IconKeys;
  leftSection?: ReactNode;
  arrow?: boolean;
  rightSection?: ReactNode;
}

const CardButton: React.FC<CardButtonProps> = ({ title, subtitle, leftSection, leftButtonIcon, arrow, rightSection, ...props }) => (
  <StyledButton background="#fff" {...props}>
    { leftButtonIcon && <StyledIconButton icon={leftButtonIcon} /> }
    { leftSection && leftSection }

    <Content>
      <Title className="title">{title}</Title>
      { subtitle && <span>{subtitle}</span> }
    </Content>

    { arrow && <StyledIcon icon="arrow" /> }
    { rightSection && rightSection }
  </StyledButton>
);

export default CardButton;
