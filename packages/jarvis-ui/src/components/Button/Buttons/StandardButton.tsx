import styled from 'styled-components';
import { ButtonProps } from '../types';
import { generateButtonStyles } from '../helpers';

export default styled.button<ButtonProps>`
  ${(props) => generateButtonStyles(props) }
  outline: none;
`;
