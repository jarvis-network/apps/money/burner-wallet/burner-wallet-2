import React from 'react';
import styled from 'styled-components';
import Button from '../Button';
import { ButtonProps } from '../types';
import Icon from '../../Icon';
import { IconKeys } from '../../Icon/types';

const StyledButton = styled(Button)`
  width: 55px;
  height: 55px;
  padding: 10px;
`;

const StyledIcon = styled(Icon)`
  width: 100%;
`;

interface IconButtonProps extends ButtonProps {
  icon: IconKeys;
}

const IconButton: React.FC<IconButtonProps> = ({ icon, ...props }) =>
  <StyledButton {...props}><StyledIcon icon={icon!} /></StyledButton>;

export default IconButton;
