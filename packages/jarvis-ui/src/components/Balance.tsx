import React, { useEffect } from 'react';
import styled from 'styled-components';
import { DataProviders, useBurner } from '@burner-wallet/ui-core';
import { faSpinner } from '@fortawesome/free-solid-svg-icons';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';

const Container = styled.div`
  display: flex;
  justify-content: center;
  padding: 40px 0;
`;

const Loader = styled(FontAwesomeIcon)`
  height: 40px;
`;

const DisplayedBalance = styled.h1`
  font-size: 34px;
  margin: 0;
`;

const Fractional = styled.span`
  color: #d9d9d9;
`;

const { AccountBalance } = DataProviders;

interface BalanceProps {
  onChange?: (balance: string) => void;
}

const Balance: React.FC<BalanceProps> = ({ onChange, ...props }) => {
  return (
    <AccountBalance
      asset="dai"
      decimals={4}
      render={data => {
        if (data && onChange)
          onChange(data.balance);

        return (
          <Container {...props}>
            {!data ?
              <Loader spin icon={faSpinner}/> :
              <DisplayedBalance>
                ${data.usdBalance!.substring(0, data.usdBalance!.length - 2)}
                <Fractional>{data.usdBalance!.slice(-2)}</Fractional>
              </DisplayedBalance>
            }
          </Container>
        );
      }}
    />
  );
};

export default Balance;
