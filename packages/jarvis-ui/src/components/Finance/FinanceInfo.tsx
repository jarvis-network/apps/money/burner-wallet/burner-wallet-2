import React from 'react';
import styled from 'styled-components';
import FinanceLabel from './FinanceLabel';

const Container = styled.div`
  width: 100%;
  font-size: 14px;
  display: flex;
  flex-direction: column;
  padding: 20px 0;
`;

interface FinanceInfoProps {
  label: string;
  amount: string;
}

const FinanceInfo: React.FC<FinanceInfoProps> = ({ label, amount }) => {
  return (
    <Container>
      <FinanceLabel>{ label }</FinanceLabel>
      <strong>{ amount } Dai</strong>
    </Container>
  )
};

export default FinanceInfo;
