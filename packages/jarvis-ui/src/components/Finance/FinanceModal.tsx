import React, { ReactNode, useEffect } from 'react';
import styled from 'styled-components';
import media, { sizes } from '../../styles/media';
import { EmojiKeys } from '../Emoji/types';
import Emoji from '../Emoji';
import useKeyPressed from '../../hooks/useKeyPressed';
import IconButton from '../Button/Buttons/IconButton';
import Page from '../Page';
import { AnimatePresence, motion } from 'framer-motion';

const pageMargin = `${sizes.page.margin}px`;

const Overlay = styled(motion.div)`
  position: fixed;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  width: 100%;
  height: 100%;
  background: #fff;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: center;
  background: rgba(0, 0, 0, 0.4);
  padding-top: ${ sizes.header.height }px;

  ${ media.mobile.default } {
    position: fixed;
  }
`;

const PageContainer = styled(Page)`
  background: transparent;

  div:first-of-type {
    align-items: center;
    justify-content: flex-end;
  }
`;

const Container = styled(motion.div)<{ borderColor: string; }>`
  border-left: 10px solid ${({borderColor}) => borderColor};
  width: 100%;
  background: #fff;
  display: flex;
  flex-direction: column;
  align-items: center;
  height: 475px;
  max-width: 320px;
`;

const Wrapper = styled.div`
  padding: ${ pageMargin };
  display: flex;
  width: 100%;
  max-width: 450px;
  flex-direction: column;
  flex: 1;
`;

const Header = styled.div`
  width: 100%;
  padding: 70px 0;
  text-align: center;
  font-size: 18px;
  font-weight: bold;
  position: relative;
`;

const StyledEmoji = styled(Emoji)`
  font-size: 24px;
`;

const Extra = styled.div`
  position: absolute;
  top: 10px;
  right: 10px;

  .close-button, .close-button img {
    padding: 0;
    width: 12px;
    height: 12px;
  }
`;

const Footer = styled.div`
  flex: 1;
  display: flex;
  align-items: flex-end;
`;

export interface FinanceModalProps {
  isVisible: boolean;
  onClose: () => void;
}

interface FinanceModalInternalProps extends FinanceModalProps {
  header: string;
  extra?: ReactNode;
  footer?: ReactNode;
  content: ReactNode;
  emoji: EmojiKeys;
  borderColor: string;
}

const FinanceModal: React.FC<FinanceModalInternalProps> = ({ onClose, isVisible, borderColor, header, emoji, extra, content, footer }) => {
  const escaped = useKeyPressed('escape');

  useEffect(() => {
    if (escaped)
      onClose();
  }, [escaped]);

  return (
    <AnimatePresence>
      {isVisible &&
      <Overlay
        onClick={() => onClose()}
        initial={{opacity: 0}}
        animate={{opacity: 1, transition:{ duration: 0.2 }}}
        exit={{opacity: 0}}
        key="modal"
      >
        <PageContainer variant="fullscreen">
          <Container
            inherit
            initial={{opacity: 0, y: 500}}
            animate={{
              opacity: 1,
              y: 0,
              transition: {
                type: 'spring',
                damping: 100,
                stiffness: 100
              }
            }}
            exit={{opacity: 0, y: 500}}
            key="container"
            borderColor={borderColor}
            onClick={e => e.stopPropagation()}
          >
            <Header>
              <StyledEmoji emoji={emoji}/> {header}
              <Extra>
                {!!extra ? extra :
                  <IconButton
                    className="close-button"
                    background="transparent"
                    icon="close"
                    onClick={onClose}
                  />
                }
              </Extra>
            </Header>
            <Wrapper>
              {content}
              {
                !!footer &&
                <Footer>
                  {footer}
                </Footer>
              }
            </Wrapper>
          </Container>
        </PageContainer>
      </Overlay>
      }
    </AnimatePresence>
  )
};

export default FinanceModal;
