import React from 'react';
import styled from 'styled-components';

const Label = styled.div<{ large?: boolean; }>`
  font-size: 14px;
  font-weight: lighter;
  ${({ large }) => large ? 'font-size: 18px; margin-bottom: 55px; text-align: center;' : ''}
`;

interface FinanceLabelProps {
  large?: boolean;
}

const FinanceLabel: React.FC<FinanceLabelProps> = ({ large, children, ...props }) => <Label large={large} {...props}>{ children }</Label>;

export default FinanceLabel;
