import React, { ReactNode } from 'react';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import CardButton from '../Button/Buttons/CardButton';
import Emoji from '../Emoji';
import { EmojiKeys } from '../Emoji/types';

const Container = styled(motion.div)`
  height: 125px;
  min-width: 125px;
`;

const StyledButton = styled(CardButton)<{ borderColor: string; }>`
  height: 100%;
  box-shadow: var(--page-box-shadow);
  border: none;
  border-left: 8px solid red;
  padding: 5px;
  flex-direction: column;
  border-left: 10px solid ${({ borderColor}) => borderColor};

  .title {
    * { font-size: 16px; }
    font-weight: normal;
  }
`;

const StyledEmoji = styled(Emoji)<{ disabled: boolean; }>`
  margin-bottom: 15px;
  font-size: 24px;
  ${({ disabled }) => disabled ? `
    filter: grayscale(100%);
  ` : ''}
`;

const InfoIcon = styled.div<{ background?: string; }>`
  height: 13px;
  width: 13px;
  font-size: 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  align-self: flex-end;
  background: ${({ background }) => background ? background : 'transparent'};
`;

interface FinanceThumbnailProps {
  emoji: EmojiKeys;
  borderColor: string;
  title: ReactNode;
  to: string;
  icon?: ReactNode;
  iconBackground?: string;
  disabled?: boolean;
}

const FinanceThumbnail: React.FC<FinanceThumbnailProps> = ({ emoji, to, borderColor, title, icon, iconBackground, disabled = false, ...props }) => {
  return (
    <Container
      whileHover={{ scale: 1.05 }}
      whileTap={{ scale: 0.95 }}
      {...props}
    >
      <StyledButton
        borderColor={disabled ? '#404040' : borderColor}
        title={title}
        leftSection={<StyledEmoji emoji={emoji} disabled={disabled} />}
        rightSection={
          <InfoIcon background={iconBackground}>
            { icon }
          </InfoIcon>
        }
        to={`/finance/${to}`}
      />
    </Container>
  )
};

export default FinanceThumbnail;
