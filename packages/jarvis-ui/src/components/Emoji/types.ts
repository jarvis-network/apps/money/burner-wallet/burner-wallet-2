export enum Emoji {
  money_mouth = '\u{1F911}',
  camera = '\u{1F4F7}',
  police_car_light = '\u{1F6A8}',
  hammer_and_wrench = '\u{1F6E0}',
  money_bag = '\u{1F4B0}',
  seedling = '\u{1F331}',
  die = '\u{1F3B2}',
  charts = '\u{1F4CA}',
  robot = '\u{1F916}',
  house = '\u{1F3E0}',
  shield = '\u{1F6E1}',
  credit_card = '\u{1F4B3}',
  game_controller = '\u{1F579}'
}

export type EmojiKeys = keyof typeof Emoji;
