import React from 'react';
import { Emoji as EmojiList, EmojiKeys } from './types';

interface EmojiProps {
  emoji: EmojiKeys;
}

const Emoji: React.FC<EmojiProps> = ({ emoji, ...props }) => {
  return (
    <span {...props}>
      {EmojiList[emoji]}
    </span>
  );
};

export default Emoji;
