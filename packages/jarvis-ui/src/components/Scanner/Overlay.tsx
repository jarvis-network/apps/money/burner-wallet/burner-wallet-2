import React from 'react';
import styled from 'styled-components';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCircleNotch } from '@fortawesome/free-solid-svg-icons'
import { sizes } from '../../styles/media';
import Zoomer from './Zoomer';
import FallbackQRCode from './FallbackQRCode';
import Button from "../Button";

const Wrapper = styled.div`
  position: absolute;
  width: 100%;
  top: 0;
  height: 100%;
  left: 0;
  display: flex;
  flex-direction: column;

  .top, .left, .bottom, .right, .loader {
    background: rgba(0, 0, 0, 0.5);
  }

  .top {
    flex: 1;
  }

  .row {
    flex: 1;
    display: flex;
    flex-direction: row;
    min-height: 250px;

    .left {
      flex: 1;
    }

    .center {
      width: 250px;
      height: 250px;
      z-index: 10;
      display: flex;
    }

    .right {
      flex: 1;
    }
  }

  .bottom {
    display: flex;
    justify-content: center;
    align-items: center;
    flex: 1;
  }
`;

const InstructionsContainer = styled.div`
  display: flex;
  width: ${ sizes.page.content.width }px;
  justify-content: space-around;
  align-items: center;
`;

const Loader = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
  color: #fff;
`;

interface OverlayProps {
  loading: boolean;
  legacyMode: boolean;
  reader: React.RefObject<any>;
}

const Overlay: React.FC<OverlayProps> = ({ loading, legacyMode, reader }) => (
  <Wrapper>
    <div className="top" />
    <div className="row">
      <div className="left" />
      <div className="center">
        {
          loading ?
            <Loader className="loader">
              <FontAwesomeIcon
                icon={faCircleNotch}
                spin
                size="3x"
              />
            </Loader>
          :
           !legacyMode
           ?
             <Zoomer />
           :
             <FallbackQRCode />
        }
      </div>
      <div className="right" />
    </div>
    <div className="bottom">
      {
        legacyMode &&
        <InstructionsContainer>
          <span>Upload a photo of a QR code</span>
          <Button onClick={() => reader.current!.openImageDialog()}>Upload</Button>
        </InstructionsContainer>
      }
    </div>
  </Wrapper>
);

export default Overlay;
