import { Actions, Asset } from '@burner-wallet/types';
import { pkToAddress } from '../../lib';

export default class PrivateKeyManager {
  constructor(
    private privateKey: string,
    private currentAddress: string, // defaultAccount
    private asset: Asset,
    private callSigner: Actions['callSigner'],
    private canCallSigner: Actions['canCallSigner']) {
  }

  public async setKey() {
    if (!this.canSetNewPrivateKey())
      throw Error('Cannot set new private key');

    if (this.getNewAddress().toLowerCase() === this.currentAddress.toLowerCase())
      throw Error('The scanned account is the same as the current');

    try {
      if (PrivateKeyManager.isBalanceZero(await this.getBalance(this.currentAddress))) {
        await this.setNewPrivateKey(this.privateKey);
      } else {
        await this.enableTemp(this.privateKey);
        await this.moveBalanceToCurrentAddress();
        await this.disableTemp();
      }
    } catch (e) {
      throw Error('Couldn\'t transfer the balance');
    }
  }

  // Balance actions

  private async getBalance(account: string) {
    return await this.asset.getBalance(account);
  }

  private async moveBalanceToCurrentAddress() {
    const newAddress = this.getNewAddress();
    const balance = await this.asset.getMaximumSendableBalance(newAddress, this.currentAddress);

    if (!PrivateKeyManager.isBalanceZero(balance)) {
      await this.moveBalance(newAddress, this.currentAddress, balance);
    }
  }

  private async moveBalance(senderAddress: string, recipientAddress: string, value: string) {
    await this.asset.send({
      to: recipientAddress,
      from: senderAddress,
      value
    });
  }

  private getNewAddress() {
    return this.canCallSigner('keyToAddress', this.currentAddress) ?
      this.callSigner('keyToAddress', this.currentAddress, this.privateKey)
      :
      pkToAddress(this.privateKey);
  }

  // Private key actions

  private canSetNewPrivateKey() {
    return this.canCallSigner('writeKey', this.currentAddress, this.privateKey)
  }

  private async setNewPrivateKey(newPrivateKey: string) {
    return this.callSigner('writeKey', this.currentAddress, newPrivateKey);
  }

  // Temp actions

  private async enableTemp(privateKey: string) {
    return this.callSigner('enable', 'temp', privateKey);
  }

  private async disableTemp() {
    return this.callSigner('disable', 'temp');
  }

  // Static actions

  private static isBalanceZero(balance: string) {
    return balance === '0';
  }
}
