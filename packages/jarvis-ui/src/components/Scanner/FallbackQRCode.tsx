import React from 'react';
import QRCode from 'qrcode.react';
import styled from 'styled-components';
import { BurnerContext, withBurner } from '@burner-wallet/ui-core';

const StyledQRCode = styled(QRCode)`
  height: auto !important;
  width: 100% !important;
  filter: blur(1px);
`;

interface FallbackQRCodeProps extends BurnerContext {
}

const FallbackQRCode: React.FC<FallbackQRCodeProps> = ({ defaultAccount }) => (
  <StyledQRCode
    value={defaultAccount}
  />
);

export default withBurner(FallbackQRCode);
