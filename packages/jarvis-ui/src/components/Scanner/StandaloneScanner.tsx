import React, { useEffect } from 'react';
import styled from 'styled-components';
import Title from '../Page/PageTitleBar';
import media from '../../styles/media';
import Scanner, { ScannerProps } from './Scanner';
import { RegexKeys } from '../../constants/regex';
import useKeyPressed from '../../hooks/useKeyPressed';
import { useNotification } from '../Notification';

const TitleContainer = styled.div`
  position: absolute;
  top: 0;
  left: 0;
  width: 100%;
  z-index: 2;
  color: #fff;

  .page-title-close-button {
    color: #fff;
  }
`;

const Wrapper = styled.div`
  position: absolute;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  z-index: 10;

  ${ media.mobile.default } {
    position: fixed;
  }
`;

interface StandaloneScannerProps extends ScannerProps {
  title?: string;
  onClose: () => void;
}

const StandaloneScanner: React.FC<StandaloneScannerProps> = ({
  title = "Scan",
  onScan,
  onLoad,
  onClose,
  regexes,
  loading = false
}) => {
  const { pushNotification } = useNotification();
  const escaped = useKeyPressed('escape');

  useEffect(() => {
    if (!navigator.mediaDevices || !navigator.mediaDevices.enumerateDevices) {
      pushNotification({
        emoji: 'hammer_and_wrench',
        message: 'This is not optimized for this browser.',
        type: 'warning'
      });
      onClose();
    }
  }, []);

  return (
    <Wrapper>
      <TitleContainer>
        <Title title={title} to={onClose}/>
      </TitleContainer>
      <Scanner onScan={onScan} onLoad={onLoad} regexes={regexes} loading={loading} />
      { escaped && onClose() }
    </Wrapper>
  )
};

export default StandaloneScanner;
