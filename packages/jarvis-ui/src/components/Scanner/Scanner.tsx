import React, { useRef, useState } from 'react';
import QrReader from 'react-qr-reader';
import styled from 'styled-components';
import Regex, { RegexKeys } from "../../constants/regex";
import Overlay from './Overlay';
import PKManager from './privateKeyManager';
import { useBurner } from '@burner-wallet/ui-core';
import { useNotification } from '../Notification';

interface ContentContainerProps {
  readerImageLoaded: boolean;
  facemode: boolean;
}

const ContentContainer = styled.div<ContentContainerProps>`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;

  .reader {
    display: ${({ readerImageLoaded }) => !readerImageLoaded ? 'none' : 'block'};
  }

  video {
   transform: ${({ facemode }) => facemode ? 'scaleX(-1)' : 'none'};
  }
`;

const StyledQrReader = styled(QrReader)`
  width: 100%;

  section {
    position: unset !important;

    div {
      border: none !important;
      box-shadow: none !important;
    }
  }
`;

export interface ScannerProps {
  onScan?: (result: string) => void;
  onLoad?: () => void;
  regexes?: (RegexKeys | {
    name: RegexKeys;
    callback: (data: string) => void;
  })[];
  loading?: boolean;
}

const Scanner: React.FC<ScannerProps> = ({
  onScan,
  onLoad,
  regexes,
  loading = false
}: ScannerProps) => {
  const { defaultAccount, actions: { callSigner, canCallSigner, navigateTo }, assets } = useBurner();
  const dai = assets.filter(asset => asset.id === 'dai')[0];
  const { pushNotification } = useNotification();
  const reader = useRef<any>(null);
  const [isLoading, setIsLoading] = useState(true);
  const [legacyMode, setLegacyMode] = useState(false);
  const [legacyModeImageLoaded, setLegacyModeImageLoaded] = useState(false);
  const [facemode, setFacemode] = useState(false);

  return (
    <ContentContainer readerImageLoaded={(legacyModeImageLoaded || !legacyMode)} facemode={facemode}>
      <StyledQrReader
        ref={reader}
        delay={300}
        onScan={data => {
          if (data === null)
            return null;

          const defaultRegexFunctions = {
            pk: async () => {
              setIsLoading(true);
              const PK = new PKManager(data, defaultAccount, dai, callSigner, canCallSigner);
              try {
                await PK.setKey();
                navigateTo('/', {
                  notification: 'Successfully transferred the scanned account'
                });
              } catch (e) {
                pushNotification({
                  emoji: 'police_car_light',
                  message: e.message,
                  type: 'warning'
                });
              } finally {
                setIsLoading(false);
              }
            },
            address: () => {
              navigateTo('/send', {
                to: data
              });
            },
            ens: () => {
              navigateTo('/send', {
                to: data
              });
            }
          };

          if (regexes) {
            // checks whether a callback is given for each regex and call it
            // otherwise calls the onScan function which will be used for every
            // regex in the current instance or the predefined default behaviour
            // for each regex
            regexes.forEach(regex => {
              const regexName = typeof regex === 'string' ? regex : regex.name;
              const regexCallback = typeof regex === 'string' ? onScan || defaultRegexFunctions[regexName] : regex.callback;
              if (new RegExp(Regex[regexName], 'i').test(data)) {
                return regexCallback(data);
              }
            });
          } else
            onScan!(data);
        }}
        onError={() => {
          setIsLoading(false);
          setLegacyMode(true);
        }}
        // @ts-ignore (wrong react-qr-scanner types)
        onLoad={({ streamLabel }) => {
          if (/front|user|face/ig.test(streamLabel))
            setFacemode(true);
          setIsLoading(false)
        }}
        onImageLoad={() => setLegacyModeImageLoaded(true)}
        facingMode="environment"
        legacyMode={legacyMode}
        className="reader"
      />
      <Overlay
        loading={loading || isLoading}
        legacyMode={loading ? false : legacyMode}
        reader={reader}
      />
    </ContentContainer>
  );
};

export default Scanner;
