import React, { useEffect, useState } from 'react';
import { Account } from '@burner-wallet/types';
import { useBurner } from '@burner-wallet/ui-core';
import Popup from '../../../Popup';
import CardButton from '../../../Button/Buttons/CardButton';
import styled from 'styled-components';

const DropdownOption = styled(CardButton)`
  justify-content: flex-start;
  overflow: hidden;
  width: 100%;
  padding: 10px;
`;

interface AccountsDropdownProps {
  search: string;
  anchor: HTMLInputElement;
  onSelect: (account: Account) => void;
}

const AccountsDropdown: React.FC<AccountsDropdownProps> = ({ anchor, search, onSelect }) => {
  const { pluginData: { accountSearches } } = useBurner();
  const [accounts, setAccounts] = useState<Account[]>([]);

  useEffect(() => {
    let canceled = false;

    Promise.all(accountSearches.map(searchFn => searchFn(search)))
      .then((_accounts: Account[][]) => {
        if (!canceled) {
          const flattenedAccounts = Array.prototype.concat(..._accounts);
          setAccounts(flattenedAccounts);
        }
      });

    return () => {
      canceled = true;
    };
  }, [search]);

  if (accounts.length === 0) {
    return null;
  }

  return (
    <Popup anchor={anchor}>
      {accounts.map((account: Account) =>
        <DropdownOption title={account.name!} subtitle={account.address} onMouseDown={() => onSelect(account)} />
      )}
    </Popup>
  )
};

export default AccountsDropdown;
