import React, { useState, Fragment, useRef, useCallback } from 'react';
import { Account } from '@burner-wallet/types';
import Input from '../../../../components/Input';
import AccountsDropdown from './AccountsDropdown';
import { StandaloneScanner as Scanner } from '../../../../components/Scanner';
import Regex from '../../../../constants/regex';

interface AddressInputProps {
  label?: string;
  placeholder?: string;
  value: Account | null;
  onChange: (value: Account | null) => void;
}

const AddressInput: React.FC<AddressInputProps> = ({ value, onChange, ...props }) => {
  const [showScanner, setShowScanner] = useState(false);
  const [focused, setFocused] = useState(false);
  const [address, setAddress] = useState(value ? value.address : '');
  const inputRef = useRef<HTMLInputElement | null>(null);

  const handleValueChange = useCallback((account: Account | string) => {
    if (typeof account === 'string') {
      setAddress(account);
      if (new RegExp(Regex.address, 'i').test(account)) {
        onChange({
          address: account
        });
      }
    } else
      onChange(account);
  }, []);

  return (
    <Fragment>
      <Input
        ref={inputRef}
        buttonIcon={value ? 'close' : 'scan'}
        buttonBackground={value ? 'transparent' : undefined}
        onButtonClick={() => {
          if (value) {
            onChange(null);
            setAddress('');
          } else
            setShowScanner(true);
        }}
        value={address}
        onChange={handleValueChange}
        onFocus={() => setFocused(true)}
        onBlur={() => setFocused(false)}
        disabled={!!value}
        {...props}
      />
      {inputRef && focused &&
        <AccountsDropdown
          anchor={inputRef.current!}
          search={address}
          onSelect={(account: Account) => {
            setFocused(false);
            handleValueChange(account);
          }}
        />
      }
      {
        showScanner &&
        <Scanner
          onScan={address => {
            handleValueChange(address);
            setShowScanner(false);
          }}
          onClose={() => setShowScanner(false)}
          regexes={['address']}
        />
      }
    </Fragment>
  )
};

export default AddressInput;
