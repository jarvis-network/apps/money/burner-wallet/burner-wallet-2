import React  from 'react';
import styled from 'styled-components';
import IconButton from '../Button/Buttons/IconButton';
import { IconKeys } from '../Icon/types';

const InputGroupContainer = styled.div`
  display: flex;
  flex-direction: column;
  font-size: 14px;
`;

const Label = styled.label`
  margin-left: 15px;
  margin-bottom: 20px;
`;

const InputContainer = styled.div`
  position: relative;
`;

const StyledInput = styled.input<{
  button: boolean;
  invalid: boolean;
}>`
  height: 62px;
  width: 100%;
  border: 1px solid #F1F1F1;
  padding: 20px 15px;
  outline: none;
  ${({ button }) => button ? 'padding-right: 70px' : ''}
  ${({ invalid }) => invalid ? `
    box-shadow: 0 0 6px red;
    border: 1px solid red;
  ` : ''}

  ::placeholder {
    color: #BEBEBE;
  }
`;

const InputButton = styled(IconButton)`
  width: 50px;
  height: 50px;
  position: absolute;
  top: 6px;
  right: 6px;
`;

interface InputProps {
  value: any;
  onChange: (value: string) => void;
  type?: string;
  placeholder?: string;
  disabled?: boolean;
  label?: string;
  invalid?: boolean;
  buttonIcon?: IconKeys;
  buttonBackground?: string;
  onButtonClick?: () => void;
  [key: string]: any;
}

const Input: React.FC<InputProps> = React.forwardRef<HTMLInputElement, InputProps>(({
  value,
  onChange,
  type = "text",
  placeholder,
  disabled,
  label,
  invalid = false,
  buttonIcon,
  buttonBackground,
  onButtonClick,
  ...props
}, ref) => {
  return (
    <InputGroupContainer>
      {
        !!label &&
        <Label>{label}</Label>
      }
      <InputContainer>
        <StyledInput
          value={value}
          type={type}
          onChange={({ target }) => onChange(target.value)}
          placeholder={placeholder}
          disabled={disabled}
          button={!!buttonIcon}
          invalid={invalid}
          ref={ref}
          {...props}
        />
        {
          !!buttonIcon &&
          <InputButton
            icon={buttonIcon}
            onClick={onButtonClick}
            background={buttonBackground}
          />
        }
      </InputContainer>
    </InputGroupContainer>
  )
});

export default Input;
