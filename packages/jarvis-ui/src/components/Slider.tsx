import React from 'react';
import styled from 'styled-components';

interface InputProps {
  value: number;
  min: number;
  max: number;
}

const Input = styled.input<InputProps>`
  -webkit-appearance: none;
  width: 100%;
  height: 15px;
  ${({ value, min, max }) => {
    const percent = (value - min) * 100 / (max - min);
    return `background: linear-gradient(to right, #D9D9D9 0%, #D9D9D9 ${percent}%, #F1F1F1 ${percent}%, #F1F1F1 100%);`
  }}
  outline: none;

  &::-webkit-slider-thumb {
    -webkit-appearance: none;
    appearance: none;
    width: 5px;
    height: 20px;
    background: #D9D9D9;
    cursor: pointer;
  }

  &::-moz-range-thumb {
    width: 5px;
    height: 20px;
    background: #D9D9D9;
    cursor: pointer;
  }
`;

interface SliderProps {
  min: number;
  max: number;
  value: number;
  onChange: (value: number) => void;
}

const Slider: React.FC<SliderProps> = ({ min, max, value, onChange }) => {
  return (
    <Input type="range" min={min} max={max} value={value} onChange={e => onChange((e.target.value as unknown) as number)} />
  )
};

export default Slider;
