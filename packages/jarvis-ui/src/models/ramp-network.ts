import { encodeUrlParams } from '../utils/uri';

export type RampNetworkParams = {
  hostAppName?: string;
  hostLogoUrl?: string;
  hostApiKey?: string;
  swapAsset?: string;
  swapAmount?: string;
  userAddress?: string;
  userEmailAddress?: string;
  url?: string;
  variant?: 'desktop' | 'mobile' | 'auto';
  webhookStatusUrl?: string;
}

export function getRampUrl(params: RampNetworkParams) {
  const base = 'https://buy.ramp.network';
  const query = encodeUrlParams(params);
  return `${base}/?${query}`;
}
