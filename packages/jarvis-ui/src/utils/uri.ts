export function encodeUrlParams(params: { [key: string]: string | undefined }) {
  return Object.entries(params)
    .filter(entry => entry.length == 2 && entry[0] && entry[1])
    .map(kv => (kv as [string, string]).map(encodeURIComponent).join('='))
    .join('&');
}
