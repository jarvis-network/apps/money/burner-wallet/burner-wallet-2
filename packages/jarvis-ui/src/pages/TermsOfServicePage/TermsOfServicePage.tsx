import React from 'react';
import { IntroductionPage } from '../../components/Page';
import ContentMD from '!!raw-loader!./content.md';

const TermsOfServicePage: React.FC = () => (
    <IntroductionPage
      title="Terms of service"
      intro="No worry, we have made them super clean and easy to read."
      source={ContentMD}
    />
);

export default TermsOfServicePage;
