import React from 'react';
import styled from 'styled-components';
import Page from '../../components/Page';
import Transactions from '../../components/Transactions';

const PageContainer = styled(Page)`
  padding: 0;
`;

const ActivityPage: React.FC = () => {
  return (
    <PageContainer title="Recent Activity" variants={['fullcontent', 'fullscreen']} scrollable>
      <Transactions />
    </PageContainer>
  );
};

export default ActivityPage;
