import React, { useEffect } from 'react';
import styled from 'styled-components';
import { withBurner, BurnerContext } from '@burner-wallet/ui-core';
import QRCode from 'qrcode.react';
import Page from '../../components/Page';
import AddressField from './AddressField';

import DaiLogo from './dai_logo.svg';
import { useNotification } from '../../components/Notification';

const Description = styled.div`
  padding: 25px;
  font-weight: lighter;
`;

const QRContainer = styled.div`
  border: 1px solid #F1F1F1;
  padding: 20px;
  flex: 1;
  display: flex;
  flex-direction: column;
  max-height: 300px;

  & svg {
    flex: 1;
    width: initial;
    height: initial;
  }
`;

const ReceivePage: React.FC<BurnerContext> = ({ defaultAccount }) => {
  const { pushNotification } = useNotification();

  useEffect(() => {
    pushNotification({
      emoji: 'police_car_light',
      message: 'Only send DAI to this address.',
      type: 'warning',
      delayed: true,
      onlyOnce: true
    });
  }, []);

  return (
    <Page title="Receive" variant="fullscreen">
      <Description>Scan this code with another wallet or share  your address</Description>

      <QRContainer>
        <QRCode
            value={defaultAccount}
            renderAs="svg"
            imageSettings={{
                src: DaiLogo,
                height: 40,
                width: 40,
                excavate: true
            }}
            level="H"
        />
      </QRContainer>

      <AddressField />
    </Page>
  );
};

export default withBurner(ReceivePage);
