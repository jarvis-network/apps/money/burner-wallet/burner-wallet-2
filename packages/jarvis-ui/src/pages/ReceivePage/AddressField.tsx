import React, { Fragment, ReactNode } from 'react';
import { withBurner, BurnerContext } from '@burner-wallet/ui-core';
import styled from 'styled-components';
import Clipboard from '../../components/Clipboard';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faShareAlt } from '@fortawesome/free-solid-svg-icons';
import Button from '../../components/Button';

interface NavigatorShareProps {
  text: string;
  title: string;
}

interface CustomNavigator extends Navigator {
  share?: (options: NavigatorShareProps) => Promise<any>;
}

const AddressContainer = styled.div`
  display: flex;
  background-color: white;
  border: 1px solid #F1F1F1;
  margin: 8px 0;
  align-items: center;
  height: 62px;
  padding: 0 11px;

  svg {
    color: #BEBEBE;
  }

  span, button {
    font-size: 13px;
  }

  button[disabled] {
    background: transparent;
  }
`;

const Address = styled.span`
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
  flex: 1;
  color: #BEBEBE;
`;

interface ButtonWrapperProps {
  textToCopy: string;
  canShare: boolean;
  children: (isCopied: boolean) => ReactNode;
}

const ButtonWrapper: React.FC<ButtonWrapperProps> = ({ canShare, textToCopy, children }) =>
  !canShare ?
    <Clipboard text={textToCopy} >
      {isCopied => children(isCopied)}
    </Clipboard>
  :
    <Fragment>
      {children(false)}
    </Fragment>
;

interface AddressFieldProps extends BurnerContext {
}

const AddressField: React.FC<AddressFieldProps> = ({ defaultAccount }) => {
  const canShare = !!(navigator as CustomNavigator).share;

  return (
    <AddressContainer>
      <Address>{ defaultAccount.substr(0, 6) }...{ defaultAccount.substr(-4) }</Address>
      <ButtonWrapper textToCopy={defaultAccount} canShare={canShare}>
        {(isCopied: boolean) => (
          <Button
            onClick={async () => {
              if (canShare) {
                try {
                  await (navigator as CustomNavigator).share!({
                    text: defaultAccount,
                    title: 'Wallet address'
                  });
                } catch (e) {}
              }
            }}
            background={canShare ? 'transparent' : 'undefined'}
            disabled={isCopied}
          >
            {
              canShare ?
              <FontAwesomeIcon icon={faShareAlt}/>
              :
                <Fragment>{isCopied ? 'Copied' : 'Copy'}</Fragment>
            }
              </Button>

        )}
      </ButtonWrapper>
    </AddressContainer>
  );
};

export default withBurner(AddressField);
