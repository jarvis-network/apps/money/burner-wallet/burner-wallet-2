import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import styled from 'styled-components';
import media, { sizes } from '../../styles/media';
import Page from '../../components/Page';
import Button from '../../components/Button';
import WomanImage from './woman.svg';
import CardButton from '../../components/Button/Buttons/CardButton';

const PageWrapper = styled.div`
  padding: ${ sizes.page.margin }px 0;
  align-self: center;

  ${ media.mobile.default } {
     padding-left: 15px;
     padding-right: 15px;
     align-self: auto;
  }
`;

const PageContainer = styled(Page)`
    width: 316px;
    min-width: auto;
    padding: ${ sizes.page.margin }px;
    border-left: 10px solid #ff0000;
    z-index: 1;

    @media screen and (max-width: ${ 316 + sizes.page.margin * 2 }px) {
      width: 100%;
      min-width: auto;
    }
`;

const ImageContainer = styled.div`
    flex: 4;
    background: url(${WomanImage}) no-repeat center center;
    background-size: contain;

  ${ media.mobile.default } {
    flex: 2;
  }
`;

const TermsConditions = styled.div`
    flex: 6;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
`;

const TermsConditionsGreeting = styled.div`
    flex: 1;
    padding: 0 10px;
    font-weight: lighter;
    font-size: 18px;
`;

const TermsConditionsContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const TermsConditionsButtonContainer = styled.div`
  flex: 1;
`;

const TermsConditionsButton = styled(CardButton)`
  height: 75px;
  margin-bottom: 10px;
`;

const TermsAgree = styled.div`
    flex: 1;
    display: flex;
    flex-direction: row;
    align-items: flex-end;
`;

const TermsCheckboxContainer = styled.div`
    flex: 5;
    display: flex;
    flex-direction: row;
    align-items: center;
    font-size: 11px;
    font-weight: lighter;

    input {
        width: 30px;
    }

    p {
        margin: 10px;
    }
`;

const TermsCheckboxContainerButton = styled.div`
    flex: 5;
    display: flex;
    justify-content: flex-end;
`;

const AgreeButton = styled(Button)`
    font-size: 18px;
`;

const WelcomePage: React.FC = () => {
    const history = useHistory();

    const [agreed, setAgreed] = useState(false);

    return (
        <PageWrapper>
        <PageContainer variant="fullcontent">
            <ImageContainer />
            <TermsConditions>
                <TermsConditionsGreeting>
                    <p>Hello <strong>stranger!</strong></p>
                </TermsConditionsGreeting>
                <TermsConditionsContainer>
                  <TermsConditionsButtonContainer>
                      <TermsConditionsButton to="/termsofservice" title="Terms and Conditions" arrow />
                  </TermsConditionsButtonContainer>
                  <TermsConditionsButtonContainer>
                    <TermsConditionsButton to="/privacypolicy" title="Privacy Policy" arrow />
                  </TermsConditionsButtonContainer>
                </TermsConditionsContainer>
                <TermsAgree>
                    <TermsCheckboxContainer>
                        <input
                          type="checkbox"
                          checked={agreed}
                          onChange={() => setAgreed(!agreed)}
                        />
                        <p>I have understood The terms above</p>
                    </TermsCheckboxContainer>
                    <TermsCheckboxContainerButton>
                        <AgreeButton
                          type="success"
                            onClick={() => {
                                if (agreed) {
                                    localStorage.setItem('terms', 'true');
                                    history.push('/onboarding');
                                }
                            }}
                            disabled={!agreed}>
                                I agree
                        </AgreeButton>
                    </TermsCheckboxContainerButton>
                </TermsAgree>
            </TermsConditions>
        </PageContainer>
        </PageWrapper>
    )
};

export default WelcomePage;
