import React from 'react';
import { IntroductionPage } from '../../components/Page';
import ContentMD from '!!raw-loader!./content.md';

const PrivacyPolicyPage: React.FC = () => (
  <IntroductionPage
    title="Privacy policy"
    intro="In short: we are not evil, we do not store any of your data"
    source={ContentMD}
  />
);

export default PrivacyPolicyPage;
