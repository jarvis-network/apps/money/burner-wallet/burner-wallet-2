import React from 'react';
import { useParams, useHistory } from 'react-router-dom';
import styled from 'styled-components';
import Page from '../../components/Page';
import { ContentContainer } from '../../styles/components';
import Balance from '../../components/Balance';
import Buttons from './Buttons';
import RecentActivity from './RecentActivity';
import Finance from './Finance';
import { sections, SectionName } from './Finance/Sections';

const PageContainer = styled(Page)`
  padding: 0px;
`;

const Content = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  flex-direction: column;
`;

const NavigationWrapper = styled.div`
  width: 100%;
  display: flex;
  flex-direction: column;
  align-items: center;
  background: #F9F9F9;
  flex: 1;
`;

const NavigationContainer = styled(ContentContainer)`
  flex: 1;
  justify-content: space-around;
  display: flex;
  flex-direction: column;
`;

const StyledBalance = styled(Balance)`
  padding: 0;
`;

const Row = styled.div`
  flex: 1;
  display: flex;
  justify-content: center;
  align-items: center;
`;

const HomePage: React.FC = () => {
  const { financeSection } = useParams();
  const history = useHistory();

  return (
    <PageContainer variant="fullcontent">
      <Content>
        <NavigationWrapper>
          <NavigationContainer>
            <Row>
             <StyledBalance />
            </Row>
            <Buttons />
          </NavigationContainer>
        </NavigationWrapper>
        <Finance />
        <RecentActivity />
      </Content>
      {Object.keys(sections).map(sectionName => {
        const Section = sections[sectionName as SectionName];

        return <Section onClose={() => history.goBack()} isVisible={financeSection === sectionName} />
      })}
    </PageContainer>
  );
};

export default HomePage;
