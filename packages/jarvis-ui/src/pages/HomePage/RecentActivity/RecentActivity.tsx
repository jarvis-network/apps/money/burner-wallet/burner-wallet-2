import React, { Fragment } from 'react';
import Header from './Header';
import Transactions from '../../../components/Transactions';

const RecentActivity = () => (
  <Fragment>
    <Header />
    <Transactions limit={1} />
  </Fragment>
);

export default RecentActivity;
