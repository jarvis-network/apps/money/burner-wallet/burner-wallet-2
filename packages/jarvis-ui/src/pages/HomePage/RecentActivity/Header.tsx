import React from 'react';
import styled from 'styled-components';
import HeaderTitle from '../HeaderTitle';
import CardButton from '../../../components/Button/Buttons/CardButton';

const ViewAllButton = styled(CardButton)`
  height: 22px;
  padding: 3px;
  border: none;

  .title {
    align-self: center;
    font-weight: normal;
  }
`;

const Header: React.FC = () => (
  <HeaderTitle title="Recent activity">
    <ViewAllButton
      background={undefined}
      arrow
      to="/activity"
    />
  </HeaderTitle>
);

export default Header;
