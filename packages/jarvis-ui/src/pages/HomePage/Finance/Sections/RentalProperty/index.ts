import RentalProperty from './RentalProperty';
import RentalPropertyThumbnail from './RentalPropertyThumbnail';
export default RentalProperty;
export {
  RentalPropertyThumbnail
}
