import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const RentalPropertyThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="house"
      borderColor="#404040"
      title={<FinanceLabel>Invest in <strong>rental property</strong></FinanceLabel>}
      to="rentalproperty"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default RentalPropertyThumbnail;
