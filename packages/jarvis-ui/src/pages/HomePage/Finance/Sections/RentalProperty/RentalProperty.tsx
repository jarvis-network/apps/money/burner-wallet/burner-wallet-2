import React, { Fragment } from 'react';
import FinanceModal, { FinanceModalProps } from '../../../../../components/Finance/FinanceModal';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';

const RentalProperty: React.FC<FinanceModalProps> = props => {
  return (
    <FinanceModal
      borderColor="#404040"
      emoji="house"
      header="Rental property"
      content={
        <Fragment>
          <FinanceLabel large>Invest from one dollar in rental property, and receive the rents everyday.</FinanceLabel>
          <FinanceLabel large><strong>Only available by opening an account at jarvis.money</strong></FinanceLabel>
        </Fragment>
      }
      {...props}
    />
  )
};

export default RentalProperty;
