import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const LotteryThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="die"
      borderColor="#404040"
      title={<FinanceLabel>Play a <strong>no-Loss lottery</strong></FinanceLabel>}
      to="lottery"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default LotteryThumbnail;
