import React, { Fragment } from 'react';
import FinanceModal, { FinanceModalProps } from '../../../../../components/Finance/FinanceModal';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';

const Lottery: React.FC<FinanceModalProps> = props => {
  return (
    <FinanceModal
      borderColor="#404040"
      emoji="die"
      header="No-Loss lottery"
      content={
        <Fragment>
          <FinanceLabel large>Participate to a no los lottery: if you lose, your ticket is reimbursed.</FinanceLabel>
          <FinanceLabel large><strong>Only available by opening an account at jarvis.money</strong></FinanceLabel>
        </Fragment>
      }
      {...props}
    />
  )
};

export default Lottery;
