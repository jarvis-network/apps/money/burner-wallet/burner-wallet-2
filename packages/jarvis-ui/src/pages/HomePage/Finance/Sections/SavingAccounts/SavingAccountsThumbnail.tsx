import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const SavingAccountsThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="seedling"
      borderColor="#54FB77"
      title={<FinanceLabel>Save and <strong>earn interest</strong></FinanceLabel>}
      to="savingaccounts"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default SavingAccountsThumbnail;
