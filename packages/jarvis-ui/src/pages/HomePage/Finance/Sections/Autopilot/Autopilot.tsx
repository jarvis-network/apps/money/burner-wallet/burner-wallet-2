import React, { Fragment } from 'react';
import FinanceModal, { FinanceModalProps } from '../../../../../components/Finance/FinanceModal';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';

const Autopilot: React.FC<FinanceModalProps> = props => {
  return (
    <FinanceModal
      borderColor="#404040"
      emoji="robot"
      header="Invest in autopilot"
      content={
        <Fragment>
          <FinanceLabel large>Pick-up bot or experienced traders to manage your portfolio for you.</FinanceLabel>
          <FinanceLabel large><strong>Only available by opening an account at jarvis.money</strong></FinanceLabel>
        </Fragment>
      }
      {...props}
    />
  )
};

export default Autopilot;
