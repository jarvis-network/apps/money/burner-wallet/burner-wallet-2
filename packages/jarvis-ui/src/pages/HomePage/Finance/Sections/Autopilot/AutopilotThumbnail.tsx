import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const AutopilotThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="robot"
      borderColor="#404040"
      title={<FinanceLabel>Invest in crypto in <strong>autopilot</strong></FinanceLabel>}
      to="autopilot"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default AutopilotThumbnail;
