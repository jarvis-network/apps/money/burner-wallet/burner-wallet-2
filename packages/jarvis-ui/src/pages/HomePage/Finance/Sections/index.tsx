import Autopilot, { AutopilotThumbnail } from './Autopilot';
import InstantCredit, { InstantCreditThumbnail } from './InstantCredit';
import Insurance, { InsuranceThumbnail } from './Insurance';
import Lottery, { LotteryThumbnail } from './Lottery';
import RentalProperty, { RentalPropertyThumbnail } from './RentalProperty';
import SavingAccounts, { SavingAccountsThumbnail } from './SavingAccounts';
import StocksAndGold, { StocksAndGoldThumbnail } from './StocksAndGold';
import VideoGames, { VideoGamesThumbnail } from './VideoGames';

export const sections = {
  autopilot: Autopilot,
  instantcredit: InstantCredit,
  insurance: Insurance,
  lottery: Lottery,
  rentalproperty: RentalProperty,
  savingaccounts: SavingAccounts,
  stocksandgold: StocksAndGold,
  videogames: VideoGames
};

export const thumbnails = [
  SavingAccountsThumbnail,
  LotteryThumbnail,
  StocksAndGoldThumbnail,
  AutopilotThumbnail,
  RentalPropertyThumbnail,
  InsuranceThumbnail,
  InstantCreditThumbnail,
  VideoGamesThumbnail
];

export type SectionName =
  'autopilot' |
  'instantcredit' |
  'insurance' |
  'lottery' |
  'rentalproperty' |
  'savingaccounts' |
  'stocksandgold' |
  'videogames';
