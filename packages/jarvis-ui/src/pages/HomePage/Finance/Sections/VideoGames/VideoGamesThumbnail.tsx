import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const VideoGamesThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="game_controller"
      borderColor="#404040"
      title={<FinanceLabel>Play <strong>video games</strong></FinanceLabel>}
      to="videogames"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default VideoGamesThumbnail;
