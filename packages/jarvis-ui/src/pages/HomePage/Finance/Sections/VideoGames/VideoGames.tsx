import React, { Fragment } from 'react';
import FinanceModal, { FinanceModalProps } from '../../../../../components/Finance/FinanceModal';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';

const VideoGames: React.FC<FinanceModalProps> = props => {
  return (
    <FinanceModal
      borderColor="#404040"
      emoji="game_controller"
      header="Video games"
      content={
        <Fragment>
          <FinanceLabel large>Play video games, earn reward, and collect rare items that you can monetize.</FinanceLabel>
          <FinanceLabel large><strong>Only available by opening an account at jarvis.money</strong></FinanceLabel>
        </Fragment>
      }
      {...props}
    />
  )
};

export default VideoGames;
