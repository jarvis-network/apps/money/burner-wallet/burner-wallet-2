import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const InsuranceThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="shield"
      borderColor="#404040"
      title={<FinanceLabel><strong>Insure</strong> your portfolio</FinanceLabel>}
      to="insurance"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default InsuranceThumbnail;
