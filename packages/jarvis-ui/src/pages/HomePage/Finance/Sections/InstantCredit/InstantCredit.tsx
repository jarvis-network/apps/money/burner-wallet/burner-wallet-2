import React, { Fragment } from 'react';
import FinanceModal, { FinanceModalProps } from '../../../../../components/Finance/FinanceModal';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';

const InstantCredit: React.FC<FinanceModalProps> = props => {
  return (
    <FinanceModal
      borderColor="#404040"
      emoji="credit_card"
      header="Instant credit"
      content={
        <Fragment>
          <FinanceLabel large>Instantly borrow money without paperwork by using your asset as collateral.</FinanceLabel>
          <FinanceLabel large><strong>Only available by opening an account at jarvis.money</strong></FinanceLabel>
        </Fragment>
      }
      {...props}
    />
  )
};

export default InstantCredit;
