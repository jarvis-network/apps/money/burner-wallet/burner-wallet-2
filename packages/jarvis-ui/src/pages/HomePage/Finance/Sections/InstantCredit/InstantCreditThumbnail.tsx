import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const InstantCreditThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="credit_card"
      borderColor="#404040"
      title={<FinanceLabel><strong>Instant</strong> credit</FinanceLabel>}
      to="instantcredit"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default InstantCreditThumbnail;
