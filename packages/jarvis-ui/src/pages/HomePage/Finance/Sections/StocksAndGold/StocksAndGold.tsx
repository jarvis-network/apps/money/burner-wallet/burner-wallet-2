import React, { Fragment } from 'react';
import FinanceModal, { FinanceModalProps } from '../../../../../components/Finance/FinanceModal';
import FinanceInfo from '../../../../../components/Finance/FinanceInfo';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';

const StocksAndGold: React.FC<FinanceModalProps> = props => {
  return (
    <FinanceModal
      borderColor="#404040"
      emoji="charts"
      header="Stocks and Gold"
      content={
        <Fragment>
          <FinanceLabel large>Create and manage traditional and crypto assets portofolio in few clicks</FinanceLabel>
          <FinanceLabel large><strong>Only available by opening an account at jarvis.money</strong></FinanceLabel>
        </Fragment>
      }
      {...props}
    />
  )
};

export default StocksAndGold;
