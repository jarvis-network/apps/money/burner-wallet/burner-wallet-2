import React from 'react';
import FinanceThumbnail
  from '../../../../../components/Finance/FinanceThumbnail';
import FinanceLabel from '../../../../../components/Finance/FinanceLabel';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faArrowRight } from '@fortawesome/free-solid-svg-icons';

const StocksAndGoldThumbnail: React.FC = () => {
  return (
    <FinanceThumbnail
      emoji="charts"
      borderColor="#404040"
      title={<FinanceLabel>Buy <strong>stocks</strong> and <strong>gold</strong></FinanceLabel>}
      to="stocksandgold"
      icon={<FontAwesomeIcon icon={faArrowRight} />}
      disabled
    />
  );
};

export default StocksAndGoldThumbnail;
