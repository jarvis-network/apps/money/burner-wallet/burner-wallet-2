import React, {
  useEffect,
  useRef,
  Fragment
} from 'react';
import Scrollbars from 'react-custom-scrollbars';
import { motion } from 'framer-motion';
import styled from 'styled-components';
import { ContentContainer } from '../../../styles/components';
import HeaderTitle from '../HeaderTitle';
import { thumbnails } from './Sections';
import { useBackground } from '../../../components/Background';

const Button = styled(motion.div)`
  padding-left: 20px;

  &:last-of-type {
    padding-right: 20px;
  }
`;

const StyledHeaderTitle = styled(HeaderTitle)`
  background: #F9F9F9;
`;

const Content = styled(ContentContainer)`
  max-width: 400px;
  position: relative;
  height: 175px;
  display: flex;
  flex-direction: column;
  align-items: center;
  overflow: hidden;
  margin-bottom: 20px;
`;

const Row = styled.div`
  position: relative;
  display: flex;
  flex-direction: row;
  height: 100%;
  width: 100%;
  align-items: center;
`;

const Finance: React.FC = () => {
  const containerRef = useRef<any>();
  const { setOffset } = useBackground();

  useEffect(() => {
    setOffset(`${containerRef.current!.getBoundingClientRect().y + containerRef.current!.getBoundingClientRect().height}px`);
  }, [containerRef.current]);

  return (
    <Fragment>
    <StyledHeaderTitle title="Finance" ref={containerRef} />
    <Content>
      <Scrollbars
        autoHide
        hideTracksWhenNotNeeded
      >
        <Row>
          {thumbnails.map((thumb, index) => {
            const buttonRef = React.createRef<any>();

            const Thumbnail = thumb;
            return (
              <Button
                ref={buttonRef}
                key={index}
                variants={{
                  visible: i => ({
                    opacity: 1,
                    x: 0,
                    transition: {
                      duration: 0.5,
                      delay: i * 0.2
                    }
                  }),
                  hidden: {
                    x: 100,
                    opacity: 0
                  }
                }}
                initial="hidden"
                animate="visible"
                custom={index}
              >
                <Thumbnail />
              </Button>
            )
          }
        )}
        </Row>
      </Scrollbars>
    </Content>
    </Fragment>
  )
};

export default Finance;
