import React, { forwardRef, ReactNode, RefObject } from 'react';
import styled from 'styled-components';
import { ContentContainer } from '../../styles/components';

const Container = styled.div`
  width: 100%;
  display: flex;
  justify-content: center;
  border-bottom: 1px solid #f1f1f1;
`;

const Wrapper = styled(ContentContainer)`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  position: relative;
  padding-bottom: 10px;
`;

const Title = styled.div`
  margin: 0;
  font-weight: bold;
  font-size: 16px;
`;

const TabPointer = styled.div`
  position: absolute;
  width: 30px;
  border-bottom: 3px solid #54FB77;
  bottom: -2px;
  left: 30px;
`;

interface HeaderTitleProps {
  title: string;
  children?: ReactNode;
}

const HeaderTitle = forwardRef<HTMLDivElement, HeaderTitleProps>(({ title, children, ...props }, ref) => (
  <Container {...props} ref={ref}>
    <Wrapper>
      <Title>{ title }</Title>
      { children }
      <TabPointer />
    </Wrapper>
  </Container>
));

export default HeaderTitle;
