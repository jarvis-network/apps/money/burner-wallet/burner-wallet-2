import React from 'react';
import styled from 'styled-components';

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const Label = styled.span`
  font-size: 12px;
  margin-top: 10px;
`;

interface ButtonProps {
  label: string;
}

const ButtonWrapper: React.FC<ButtonProps> = ({ label, children }) => {
  return (
    <Wrapper>
      {children}
      <Label>{ label }</Label>
    </Wrapper>
  )
};

export default ButtonWrapper;
