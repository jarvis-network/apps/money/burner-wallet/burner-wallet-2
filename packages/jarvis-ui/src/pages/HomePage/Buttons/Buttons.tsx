import React, { useState } from 'react';
import IconButton from '../../../components/Button/Buttons/IconButton';
import styled from 'styled-components';
import { motion } from 'framer-motion';
import ButtonWrapper from './ButtonWrapper';
import { StandaloneScanner as Scanner } from '../../../components/Scanner';
import { useNotification } from '../../../components/Notification';
import useRampNetwork from '../../../hooks/useRampNetwork';

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: row;
  justify-content: space-between;
`;

const Buttons: React.FC = () => {
  const [scanner, showScanner] = useState(false);
  const { pushNotification } = useNotification();
  const { openRamp } = useRampNetwork();

  return (
    <Container>
      {
        [
          <ButtonWrapper label="Top-up">
            <IconButton
              icon="plus"
              onClick={() => openRamp()}
            />
          </ButtonWrapper>,
          <ButtonWrapper label="Send">
            <IconButton
              icon="send"
              to="/send"
            />
          </ButtonWrapper>,
          <ButtonWrapper label="Receive">
            <IconButton
              icon="download"
              to="/receive"
            />
          </ButtonWrapper>,
          <ButtonWrapper label="Scan">
            <IconButton
              icon="scan"
              onLoad={() =>
                pushNotification({
                  emoji: 'camera',
                  message: 'Scan an address or a magic code!',
                  type: 'info',
                  delayed: true,
                  onlyOnce: true
                })
              }
              onClick={() => showScanner(true)}
            />
          </ButtonWrapper>
        ].map((button, index) => (
          <motion.div
            key={index}
            variants={{
              visible: i => ({
                opacity: 1,
                x: 0,
                transition: {
                  duration: 0.5,
                  delay: i * 0.2
                }
              }),
              hidden: {
                x: 100,
                opacity: 0
              }
            }}
            initial="hidden"
            animate="visible"
            custom={index}
          >
            { button }
          </motion.div>
        ))
      }

      {scanner &&
        <Scanner
          onClose={() => showScanner(false)}
          regexes={['address', 'pk']}
        />
      }
    </Container>
  )
};

export default Buttons;
