import React, { useState } from 'react';
import { RouteComponentProps } from 'react-router-dom';
import { Account } from '@burner-wallet/types';
import Page from '../../components/Page';
import TransactionDetails from './steps/TransactionDetails';
import Resume from './steps/Resume';

enum Step {
  TRANSACTION_DETAILS,
  RESUME
}

const ASSET = 'dai';

const SendPage: React.FC<RouteComponentProps> = (props) => {
  const [step, setStep] = useState(Step.TRANSACTION_DETAILS);
  const [amount, setAmount] = useState('');
  const [account, setAccount] = useState<Account | null>(null);

  return (
    <Page variant="fullscreen" title="Send">
      {(() => {
        switch (step) {
          case Step.TRANSACTION_DETAILS:
            return (
              <TransactionDetails
                assetId={ASSET}
                onNext={(account, amount) => {
                  setAccount(account);
                  setAmount(amount);
                  setStep(Step.RESUME);
                }}
                {...props}
              />
            );
          case Step.RESUME:
            return (
              <Resume
                assetId={ASSET}
                amount={amount}
                account={account!}
                {...props}
              />
            );
        }
      })()}
    </Page>
  );
};

export default SendPage;
