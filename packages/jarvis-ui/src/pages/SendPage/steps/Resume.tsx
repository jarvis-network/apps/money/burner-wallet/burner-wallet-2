import React, {Fragment, useState} from 'react';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import { Account } from '@burner-wallet/types';
import { BurnerContext, withBurner } from '@burner-wallet/ui-core';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSpinner } from '@fortawesome/free-solid-svg-icons'
import Button from "../../../components/Button";
import WomanImage from './woman.svg';

const ImageContainer = styled.div`
  flex: 4;
  background: url(${ WomanImage }) no-repeat center center;
  background-size: contain;
`;

const ListContainer = styled.div`
  flex: 4;
  display: flex;
  flex-direction: column;
  justify-content: flex-end;

  p {
    font-size: 13px;
  }
`;

const List = styled.div`
  border: 1px solid #F1F1F1;
`;

const ListItem = styled.div`
  font-size: 11px;
  color: #9D9D9D;
  padding: 20px 45px;
  border-bottom: 1px solid #F1F1F1;

  &:last-child {
    border-bottom: none;
  }

  span {
    color: #000;
  }
`;

const ButtonContainer = styled.div`
  flex: 2;
  display: flex;
  align-items: flex-end;
`;

const SendButton = styled(Button)`
  background: transparent linear-gradient(90deg, #54FB77 0%, #19CEE8 100%) 0% 0% no-repeat padding-box;
  box-shadow: 0px 3px 6px #00000048;
  width: 100%;
  font-weight: normal;
  font-size: 18px;
`;

export interface ResumeProps extends BurnerContext, RouteComponentProps {
  assetId: string;
  amount: string;
  account: Account;
}

const Resume: React.FC<ResumeProps> = ({
  amount,
  account,
  assets,
  assetId,
  actions,
  defaultAccount,
  history
}) => {
  const [asset] = assets.filter(a => a.id === assetId);
  const [sending, setSending] = useState(false);

  const send = async () => {
    setSending(true);
    try {
      const receipt = await asset.send({ from: defaultAccount, to: account.address, ether: amount, value: amount });
      history.push(`/receipt/${asset.id}/${receipt.transactionHash}`);
    } catch (err) {
      setSending(false);
    }
  };

  return (
    <Fragment>
      <ImageContainer />
      <ListContainer>
        <p>Resume</p>
        <List>
          <ListItem>
            <span>Amount:</span> { amount } DAI
          </ListItem>
          <ListItem>
            <span>To:</span> { account.name ? account.name : account.address }
          </ListItem>
        </List>
      </ListContainer>
      <ButtonContainer>
        <SendButton disabled={sending} onClick={() => send()}>{ sending ? <FontAwesomeIcon spin icon={faSpinner} /> : 'SEND' }</SendButton>
      </ButtonContainer>
    </Fragment>
  );
};

export default withBurner(Resume);
