import React, {
  useState,
  Fragment
} from 'react';
import { RouteComponentProps } from 'react-router-dom';
import styled from 'styled-components';
import { DataProviders } from "@burner-wallet/ui-core";
import { Account } from '@burner-wallet/types';
import Input, { AddressInput } from '../../../components/Input';
import Button from "../../../components/Button";
import useInput from "../../../hooks/useInput";

const InputsContainer = styled.div`
  flex: 7;
  display: flex;
  flex-direction: column;
  justify-content: space-evenly;
`;

const ButtonContainer = styled.div`
  flex: 3;
  display: flex;
  justify-content: flex-end;
  align-items: flex-end;
`;

const NextButton = styled(Button)`
    width: 100px;
`;

export interface TransactionDetailsProps extends RouteComponentProps {
  assetId: string;
  onNext: (account: Account, amount: string) => void;
}

const { AccountBalance } = DataProviders;

const TransactionDetails: React.FC<TransactionDetailsProps> = ({ onNext, assetId, location }) => {
  const [account, setAccount] = useState<Account | null>(
    location.state && (location.state as any).to ? {
      address: location.state && (location.state as any).to || ''
    } : null);
  const [amountInput, amount] = useInput<string>({
    initialValue: '',
    transform: value => {
      let transformed = value
        .replace(/[^0-9|\.]/g, '')
        .replace(/^0+(?=\d)/, '')
        .replace(',', '.');

      if (transformed.indexOf('.') !== transformed.lastIndexOf('.'))
        transformed = transformed.replace(/\.(?!.*\.)/g, '');

      transformed = transformed.match(/(\d*)(\.\d{0,4})?/g)![0];

      return transformed;
    }
  });

  return (
    <AccountBalance
      asset={assetId}
      render={(data) => {

        const exceedsBalance = !!data &&
          parseFloat(amount) > parseFloat(data.displayMaximumSendableBalance);
        const canSend = account && parseFloat(amount) > 0;

        return (
          <Fragment>
            <InputsContainer>
              <AddressInput
                label="Send Dai to"
                placeholder="Type ENS, address"
                value={account}
                onChange={(account) => setAccount(account)}
              />
              <Input
                type="text"
                inputMode="decimal"
                label="How much do you want to send?"
                placeholder="Type amount"
                {...amountInput}
              />
            </InputsContainer>
            <ButtonContainer>
              <NextButton
                type="success"
                disabled={!canSend || exceedsBalance}
                onClick={() => onNext(account!, amount)}
              >
                OK
              </NextButton>
            </ButtonContainer>
          </Fragment>
        );
      }}
    />
  );
};

export default TransactionDetails;
