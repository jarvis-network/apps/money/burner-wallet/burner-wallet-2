import React from 'react';
import { useParams } from 'react-router-dom';
import styled from 'styled-components';
import { BurnerContext, withBurner, DataProviders } from '@burner-wallet/ui-core';
import { Asset, SendData } from '@burner-wallet/types';
import Address from '../../components/Address';
import Button from '../../components/Button';
import Page from '../../components/Page';
import LineItem from '../../components/LineItem';
const { TransactionDetails } = DataProviders;

interface MatchParams {
  asset: string;
  txHash: string;
}

const BigEmoji = styled.div`
  font-size: 106px;
`;

const formatDate = (timestamp: number) => (new Date(timestamp * 1000)).toLocaleString();

const ReceiptPage: React.FC<BurnerContext> = ({
  defaultAccount, assets, t
}) => {
  const { asset, txHash } = useParams<MatchParams>();
  return (
    <Page title={t('Transaction Receipt')}>
      <TransactionDetails
        asset={asset}
        txHash={txHash}
        render={(tx: SendData) => {
          if (!tx) {
            return (
              <section>
                <BigEmoji>🔎</BigEmoji>
                <div>Transaction not found...</div>
                <div>The transaction may still be propogating</div>
              </section>
            );
          }
          const [asset] = assets.filter((_asset: Asset) => _asset.id === tx.asset);
          const amtValue = asset
            ? `${asset.getDisplayValue(tx.value!)} ${asset.name}`
            : `${tx.displayValue} (unknown asset)`;

          const date = formatDate(tx.timestamp!);

          const isSent = defaultAccount.toLowerCase() === tx.from!.toLowerCase();
          return (
            <section>
              <div>
                <LineItem name={t('From')} value={<Address address={tx.from!} />}/>
                <LineItem name={t('To')} value={<Address address={tx.to!} />}/>
                <LineItem name={t('Date')} value={date}/>
                <LineItem name={isSent ? t('Sent') : t('Received')} value={amtValue}/>
              </div>

              {tx.message && (
                <div>
                  <h2>{t('Note')}</h2>
                  <div>{tx.message}</div>
                </div>
              )}
            </section>
          );
        }}
      />
    </Page>
  );
}

export default withBurner(ReceiptPage);
