import React, { useEffect, useState } from 'react';
import { BurnerContext, useBurner } from '@burner-wallet/ui-core';
import styled from 'styled-components';
import Page from "../../components/Page";
import { useNotification } from '../../components/Notification';
import CardButton from '../../components/Button/Buttons/CardButton';
import { StandaloneScanner as Scanner } from '../../components/Scanner';
import Balance from '../../components/Balance';
import useRampNetwork from '../../hooks/useRampNetwork';

const Button = styled(CardButton)`
  margin-bottom: 10px;
`;

const OnboardingPage: React.FC = () => {
  const { actions: { navigateTo } } = useBurner();
  const { pushNotification } = useNotification();
  const { openRamp } = useRampNetwork();
  const [showScanner, setShowScanner] = useState(false);
  useEffect(() => {
    pushNotification({
      emoji: 'money_mouth',
      message: 'Dai is our money. 1 dai = 1 usd',
      type: 'info',
      delayed: true,
      onlyOnce: true
    });
  }, []);

  return (
    <Page>
      <Balance
        onChange={amount => {
          if (amount !== '0') {
            localStorage.setItem('funded', 'true');
            navigateTo('/');
          }
        }}
      />
      <Button leftButtonIcon="scan" title="Reedem Dai" subtitle="Scan a Magic Code" arrow onClick={() => setShowScanner(true)} />
      <Button leftButtonIcon="plus" title="Buy Dai with fiat" subtitle="Connect to your bank" arrow onClick={() => openRamp()} />
      <Button leftButtonIcon="download" title="Receive Dai" subtitle="See your address" arrow to="/receive" />

      {
        showScanner &&
        <Scanner
          onLoad={() =>
            pushNotification({
              emoji: 'camera',
              message: 'Scan a QR code to unlock the funds',
              type: 'info',
              delayed: true,
              onlyOnce: true
            })
          }
          title="Reedem"
          onClose={() => setShowScanner(false)}
          regexes={['pk']}
        />
      }
    </Page>
  );
};

export default OnboardingPage;
