import React, { Component, ComponentClass, ComponentType } from 'react';
import { Redirect, Route, RouteProps } from 'react-router-dom';

interface PublicRouteProps extends RouteProps {
  component: ComponentType<any>;
}

const PublicRoute: React.FC<PublicRouteProps> = ({ component: Component, ...props }) => {
  return (
    <Route
      render={() => !localStorage.getItem('terms')
        ? <Component {...props} />
        : <Redirect to="/" />
      }
      {...props}
    />
  )
};

export default PublicRoute;
