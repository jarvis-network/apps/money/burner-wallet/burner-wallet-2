import { Redirect, Route, RouteProps, RouteComponentProps } from 'react-router-dom';
import React, { ComponentType } from 'react';

interface PrivateRoute extends RouteProps {
  initial?: boolean;
  onlyInitial?: boolean;
  component: ComponentType<any>;
}

const PrivateRoute: React.FC<PrivateRoute> = ({ component: Component, initial, onlyInitial, ...props }) => {
  const render = () => <Component {...props} />;

  return (
    <Route
      render={() => {
        const terms = localStorage.getItem('terms');
        const funded = localStorage.getItem('funded');

        if (terms) {
          if (initial)
            return render();

          if (onlyInitial) {
            if (!funded)
              return render();
            else
              return <Redirect to="/" />;
          }

          return funded ? render() : <Redirect to="/onboarding" />;
        }

        return <Redirect to="/welcome" />;
      }}
      {...props}
    />
  )
};

export default PrivateRoute;
