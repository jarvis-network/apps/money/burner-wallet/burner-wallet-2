import React from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';
import { BurnerPluginData } from '@burner-wallet/types';
import HomePage from '../pages/HomePage';
import WelcomePage from '../pages/WelcomePage';
import TermsOfServicePage from '../pages/TermsOfServicePage';
import PrivacyPolicyPage from '../pages/PrivacyPolicyPage';
import OnboardingPage from '../pages/OnboardingPage';
import SendPage from '../pages/SendPage';
import ReceivePage from '../pages/ReceivePage/ReceivePage';
import ActivityPage from '../pages/ActivityPage';
import ReceiptPage from '../pages/ReceiptPage/ReceiptPage';
import PublicRoute from './PublicRoute';
import PrivateRoute from './PrivateRoute';

interface RouterProps {
  pluginData: BurnerPluginData;
}

const Router: React.FC<RouterProps> = ({ pluginData }) => {
  return (
    <Switch>
      <PublicRoute path="/welcome" component={WelcomePage} />
      <PublicRoute path="/termsofservice" component={TermsOfServicePage} />
      <PublicRoute path="/privacypolicy" component={PrivacyPolicyPage} />

      <PrivateRoute path="/onboarding" component={OnboardingPage} onlyInitial />

      <PrivateRoute path="/" component={HomePage} exact />
      <PrivateRoute path="/finance/:financeSection" component={HomePage} />
      <PrivateRoute path="/send" component={SendPage} />
      <PrivateRoute path="/receive" component={ReceivePage} initial />
      <PrivateRoute path="/activity" component={ActivityPage} />
      <Route path="/receipt/:asset/:txHash" component={ReceiptPage} />

      {pluginData.pages.map(({ path, Component }) => (
        <PrivateRoute path={path} key={path} component={Component} />
      ))}

      <Redirect to="/" />
    </Switch>
  );
};

export default Router;
