import styled from 'styled-components';
import { sizes } from './media';

export const ContentContainer = styled.div`
  max-width: ${ sizes.page.content.width }px;
  width: 100%;
  padding: 0 ${ sizes.page.margin }px;
`;
