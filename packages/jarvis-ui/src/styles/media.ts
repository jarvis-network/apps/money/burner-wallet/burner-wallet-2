const header = {
  height: 84
};

const page = {
  width: 690,
  height: 566,
  margin: 15,
  padding: 70,
  content: {
    width: 360
  }
};

const sizes = {
  header,
  page,
};

const media = {
  mobile: {
    height: page.height + page.margin * 2 + header.height,
    width: page.width + page.margin * 2,
    page: {
      content: {
        width: page.content.width + page.margin * 2
      }
    }
  },
  tablet: {
    width: page.width + page.padding * 2
  }
};

export default {
  mobile: {
    default: `@media screen and (max-height: ${ media.mobile.height }px), (max-width: 576px)`,
    width: `@media screen and (max-width: ${ media.mobile.width }px)`,
    content: `@media screen and (max-width: ${ media.mobile.page.content.width }px)`
  },
  tablet: {
    width: `@media screen and (max-width: ${ media.tablet.width }px)`
  }
};

export {
  sizes
};
