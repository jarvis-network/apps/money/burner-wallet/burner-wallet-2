export interface PermitTypes {
  holder: string;
  spender: string;
  nonce: string;
  expiry: string;
  allowed: boolean;
  v: string;
  r: string;
  s: string;
}
