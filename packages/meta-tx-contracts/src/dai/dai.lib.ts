import _filter from 'lodash/filter';
import { Contract } from 'web3-eth-contract';
import type { AbiItem } from 'web3-utils';
import { getWeb3Service, Web3OrService, Web3Service } from '../web3';
import { PermitTypes } from './dai.interface';
import DAI from './dai.json';

export class DAILib {
  public readonly web3Service: Web3Service;
  public abi: AbiItem[];
  private contractInstance: Contract;
  constructor(
    contractAddress: string,
    provider: Web3OrService,
  ) {
    this.abi = DAI.abi as AbiItem[];
    this.web3Service = getWeb3Service(provider);
    this.contractInstance = this.web3Service.initializeContract(
      contractAddress,
      this.abi,
    );
  }
  getFunctionSiguature(name: string) {
    return _filter(this.abi, {
      name,
    })[0].signature;
  }

  getFunctionInputs(name: string): string[] {
    return _filter(this.abi, {
      name,
    })[0].inputs.map(({ type }) => type);
  }
  decodePermitFunction(payload: string): PermitTypes {
    const inputs = this.getFunctionInputs('permit');
    const params = this.web3Service.decodePlayload(inputs, payload);
    return {
      holder: params[0],
      spender: params[1],
      nonce: params[2],
      expiry: params[3],
      allowed: params[4],
      v: params[5],
      r: params[6],
      s: params[7],
    } as PermitTypes;
  }
  async getNonce(address: string): Promise<any> {
    return await this.contractInstance.methods.nonces(address).call();
  }

  async getBalance(address: string) {
    return await this.contractInstance.methods.balanceOf(address).call();
  }
}
