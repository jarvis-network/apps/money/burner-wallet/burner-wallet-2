import { getWeb3, Web3Source } from './infura';

export class Web3Service {
  public readonly web3;
  constructor(web3OrNetwork: Web3Source) {
    this.web3 = getWeb3(web3OrNetwork);
  }

  initializeContract(contractAddres: string, abi: any) {
    return new this.web3.eth.Contract(abi, contractAddres);
  }

  decodePlayload(inputTypes: string[], payload: string) {
    return this.web3.eth.abi.decodeParameters(inputTypes, payload);
  }

  getBalance(address: string) {
    return this.web3.eth.getBalance(address);
  }

  utf8ToHex(text: string) {
    return this.web3.utils.utf8ToHex(text);
  }

  keccak256(text: string) {
    return this.web3.utils.keccak256(text);
  }

  encodeParams(types: any[], parameters: any[]) {
    return this.web3.eth.abi.encodeParameters(types, parameters);
  }

  soliditySha3(text: string) {
    return this.web3.utils.soliditySha3(text);
  }
}

export type Web3OrService = Web3Source | Web3Service;

export function getWeb3Service(web3: Web3OrService) {
  return web3 instanceof Web3Service ? web3 : new Web3Service(web3);
}
