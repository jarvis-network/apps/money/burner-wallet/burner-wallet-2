import type { AbiItem } from 'web3-utils';
import { getWeb3Service, Web3OrService, Web3Service } from '../web3';
import UniSwap from './UniswapExchangeAbi.json';

export class UniSwapExchangeLib {
  public readonly web3Service: Web3Service;
  private contractInstance;
  public abi: AbiItem[];

  constructor(
    contractAddress: string,
    provider: Web3OrService,
  ) {
    this.abi =  UniSwap as AbiItem[];
    this.web3Service = getWeb3Service(provider);
    this.contractInstance = this.web3Service.initializeContract(
      contractAddress,
      this.abi,
    );
  }
  async getTokenToEthOutputPrice(numOfTxs: number, cost: number) {
    await this.contractInstance.methods.getTokenToEthOutputPrice(
      this.web3Service.web3.utils.toBN(numOfTxs).mul(cost),
    );
  }
}
